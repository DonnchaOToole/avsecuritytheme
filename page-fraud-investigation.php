<?php get_header(); ?>
    <div class="jumbotron blacktext animated fadeInDown" id="fraudinvestigation">
        <div class="container">
            <h1>Fraud Investigation</h1>
            <p>Helping prevent fraud and breaches of security for businesses.</p>
            <a class="btn btn-default btn-lg" href="<?php
				$blog_id = get_current_blog_id();
				echo get_home_url( $blog_id, 'contact-us' ); ?>" role="button">Get in Touch »</a>
        </div>
    </div>
    <div class="container marketing animated fadeInDown justify">
        <div class="row">
            <div class="col-sm-4">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/walk-Investigation.jpg" id="fraudimagetop" class="img-responsive 2emheadspace hidden-xs">
            </div>
            <div class="col-sm-8">
                <div class="page-header">
                    <h2><i class="fa fa-lg fa-question-circle"></i> Corporate Investigations </h2>
                </div>
                <a name="corporateinvestigations"></a>
                <p>The corporate sector in Ireland can incur serious losses, not through the obvious result of a stock market crash, embezzlement or external theft, but through the indiscretion of those we should be able to trust most, our staff.</p>
                <p>AV Security can investigate either covertly or openly to establish the extent of the problem and identify those responsible. Evidence will be gathered to allow the client to pursue legal action if required.</p>
                <h4>AV Security can help you with</h4>
                <ul>
                    <li>Employee infidelity - passing of information, abuse of sickness schemes</li>
                    <li>Installation of Listening Devices</li>
                    <li>Insurance Fraud</li>
                    <li>IT Security</li>
                    <li>Due Diligence</li>
                    <li>Protection of intellectual property</li>
                    <li>Screening of new job applicants</li>
                </ul>
                <h4>Techniques that we employ:</h4>
                <ul>
                    <li>Covert surveillance</li>
                    <li>Covert photography</li>
                    <li>Undercover assignments</li>
                    <li>Evidence handling</li>
                </ul>
                Utilising the utmost discretion and tact, along with the most sophisticated covert surveillance equipment available, AV Security can provide independent evidence, including covert photographs and hidden camera footage. We subscribe to many national database information systems and are able to provide clients with the most complete and up to date information available.
                <br/><a href="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Corporate-investigations-threat-analysis.pdf" class="btn btn-primary btn-lg">Download our threat analysis brochure</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    <h2><i class="fa fa-laptop fa-lg"></i> IT Security </h2>
                </div>
                <h3>What is Computer Forensics?</h3> For an organisation to operate effectively it is imperative that its IT assets are used properly and in the interests of the organisation. However, what happens when these assets are misused or exploited, perhaps to the detriment of the organisation? Computer forensics(CF) can play a key role in protecting an organisation’s assets. Computer forensics is the practice of capturing, preserving, extracting and analysing digital information and it can provide valuable insight into activities by individuals within an organisation and ultimately gather evidence in a way that is legally admissible in a court or tribunal. AV Security offers a range of CF services to help clients retrieve evidence using appropriate experts on a range of platforms including desktop and laptop computers, servers and network shares, smart phones, PABX systems and other mobile devices. We can monitor email and internet activity.
                <h3>Why Use Computer Forensics?</h3> Computer forensic investigations can be used in the detection and prevention of crime and in any dispute where evidence is stored digitally. Traditionally, our computer forensic services have been used by financial organisations in a variety of cases including:
                <ul>
                    <li>Intellectual Property Theft</li>
                    <li>Industrial/Corporate Espionage</li>
                    <li>Employee Disputes</li>
                    <li>Fraud/Theft Investigations</li>
                    <li>Inappropriate email and internet use in the work place</li>
                </ul>
                <p>In essence, the objectives of a forensic investigation is to achieve evidence that supports the organisations concerns surrounding the leakage of information or suspected inappropriate or criminal behaviour and to ensure that any evidence gathered is admissible in a court or tribunal. Typically an investigation will start with the forensic imaging of the device which means taking a forensic copy of the device to be examined whilst the original device remains intact. A detailed examination of the imaged data can then be performed with both visible and invisible data being searched for via keyword analysis. All data extracted will be reported on providing in effect a paper trail of evidence which adheres to current ACPO Good Practice Guides for Computer-based electronic evidence to ensure that the evidence collected is admissible in court.
                </p>
            </div>
            <div class="col-md-4 hidden-sm">
                <img src="https://i.telegraph.co.uk/multimedia/archive/02609/wires_2609001b.jpg" id="frauditsecurityimage" class="2emheadspace img-responsive img-rounded ">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3><i class="fa fa-lg fa-signal"></i> Wireless Network Audits</h3>
                <p class="lead">AV Security provides customers with independent, clear, wireless network audits.</p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img id="wifi-audits-image" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/16d33610806938f355bbff36137dab092.jpg" alt="wifi-audits" class="2emheadspace img-responsive">
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <p>The analysis and tests of the network are performed as follows:</p>
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Identify access point hardware and configuration</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Verify encryption meets best practice</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Confirm that no unauthorized network access via wifi is possible</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Identify any "rogue" access point installations</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Exploits of known factory configuration weaknesses</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Exploits of known deficiency of identified network design</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Run network stumblers outside of building, determine leaking coverage</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    <h2><i class="fa fa-location-arrow fa-lg"></i> Tracking Devices</h2>
                </div>
                <p class="lead">AV Security are now able to supply and install tracking devises to monitor your staff/assets/vehicles in real time on your tablet or smartphone.</p>
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Real-time GPS protection for loved ones, assets, and vehicles of any kind.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Ideal for recreational vehicles, boats, trailers, quads and horse boxes.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Tracking devices can be used to monitor infidelity of partners.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Monitoring via our simple-to-use on-line tracking panel with iPhone app.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Allows you to monitor the speed of employees or young drivers.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Set “safe zones” around an area for children at play or around an asset or vehicle.</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Set “motion alert” and be notified on your mobile phone &amp; tracking panel when your device even moves!</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> AV Security can install a tracking device in any kind of vehicle or for quick deployment by magnetically attaching a device to an asset.</li>
                </ul>
            </div>
            <div class="col-md-4">
                <img class="img-responsive img-rounded bighead hidden-xs hidden-sm" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Google_Maps.png" />
            </div>
        </div>
    </div>
    </div>
    <?php get_footer(); ?>
