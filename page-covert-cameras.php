<?php get_header(); ?> 
    <div class="jumbotron" id="spycameras"> 
        <div class="container animated slideInLeft"> 
                    <h1>Covert Cameras</h1> 
                    <p>Our covert CCTV camera systems are designed to be unobtrusive, barely noticeable and highly effective. 
                    </p><a class="btn btn-default btn-lg" href="<?php 
        $blog_id = get_current_blog_id(); 
        echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button">Get in Touch »</a> 
             
        </div> 
    </div> 
    <div class="container"> 
        <div class="row"> 
            <div class="col-sm-6"> 
                <h2>Installation and configuration of covert camera systems for commercial or residential applications.</h2> 
                <hr /> 
                <p class="lead">Hidden cameras come in a number of inconspicuous forms that can be installed so as to be indistinguishable to an observer.</p>
                <p>These cameras are manufactured to resemble smoke alarms, motion detectors for alarm systems, clocks, thermostats for heating and air conditioning systems and many other standard items. These hidden security cameras can be connected to industry standard digital video recording systems and monitored by the owners remotely over the internet by mobile phone.</p> 
            </div> 
            <div class="col-sm-6"><img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Hidden-Cameras-Los-Angeles.jpg" class="img-responsive" />
            <img src="https://www.avsecurity.com/wp-content/uploads/2017/04/pinhole-camera.jpg" class="img-responsive" />
            </div> 
        </div> 
        <div class="page-header text-center"> 
            <h2 class="align-center">Hidden cameras can <strong>double</strong> the effectiveness of your security system</h2> 
        </div> 
        <div class="row"> 
            <div class="col-md-4"> 
                <h3>Shoplifting</h3> 
                <p>Like it or not, your losses from employee theft are likely to be almost double that from shoplifters. Some employees and customers will still be determined to steal.These individuals will look for and often find ways around your security camera system. So to deal effectively with these individuals, consider adding one or two covert cameras to your system.</p> 
            </div> 
            <div class="col-md-4"> 
                <h3>Almost invisible</h3> 
                <p>Covert cameras can be placed so that none of your employees will know they are there — giving you more freedom to keep an eye on them, and catch them in the act of stealing. In fact, one very successful strategy for using covert cameras is to deliberately leave some areas of your sales floor and storage rooms uncovered by your traditional system. This gives employees and shoplifters the idea that they can get away with stealing in those parts of the store. All the while, your covert cameras are in position to catch the unsuspecting individuals in the act.</p> 
            </div> 
            <div class="col-md-4"> 
                <h3>Designed to work with your system, or on their own.</h3> 
                <p>All of our covert cameras provide high-quality video that can be turned over to the authorities for prosecution, if needs be. Positioned properly by our engineers, covert security cameras have helped many retailers put an end to thousands of euros of theft and discourage employees from stealing in the future.</p> 
            </div> 
        </div> 
        <section>
        <div class="row">
            <div class="col-sm-4">
            <h3>Hikvision Covert Cameras</h3>
            <p class="lead">Super small, high resolution cameras.</p>
            </div>
            <div class="col-sm-8">
            
<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/eRv39dxXom8"></iframe>
</div>            </div>
        </div>
        
        </section>
        <div class="row"> 
            <div class="col-sm-8"> 
                <p class="lead">We will determine the most suitable covert camera for your environment prior to installation. We will analyse the most optimal and strategic camera angle for coverage.</p>
                <p>Having a camera alone does not ensure maximum security. The covert camera is only as good as what it can see. We will determine the correct number of cameras and their positions based on the information gathered from a site survey.</p> 
            </div> 
            <div class="col-sm-4"> 
                <img src="https://www.avsecurity.com/wp-content/uploads/2014/06/image001-2.jpg" alt="A small covert camera"> 
            </div> 
        </div> 
    </div> 
    
    </div> 
    <!--<div class="container free-quote-cctv"> 
        <div class="row"> 
            <div class="col-md-offset-3 col-md-6"> 
                <div class="col-md-6"> 
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/image001.jpg" class="img-responsive"> 
                </div> 
                <div class="col-md-6 text-center"> 
                    <p class="lead vertical-align">Get a free quote for 
                        <br/>CCTV installation</p> 
                    <p>Peace of mind is only a few clicks away.</p> 
                    <a class="btn btn-primary btn-lg" href="<?php 
        $blog_id = get_current_blog_id(); 
        echo get_home_url( $blog_id, 'contact-us' ); ?>">Get your free quote</a> 
                </div> 
            </div> 
        </div> 
    </div> -->
            <?php require_once('remote-viewing.php'); ?>

    <?php get_footer(); ?> 