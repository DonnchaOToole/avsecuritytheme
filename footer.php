<section class="footer-links">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <ul>
                    <li>
                        <a href="http://www.avsecurity.com/commercial-cctv/">Commercial CCTV</a>
                    </li>
                    <li>
                        <a href="http://www.avsecurity.com/home-cctv/">Home CCTV</a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-4">
                <ul>
                    <li>
                        <a href="http://www.avsecurity.com/covert-cameras/">Covert CCTV</a>
                    </li>
                    <li>
                        <a href="http://www.avsecurity.com/add-ons/">CCTV Add Ons</a>
                    </li>

                </ul>
            </div>
            <div class="col-sm-4">
                <ul>
                    <li>
                        <a href="http://www.avsecurity.com/cctv-upgrades/">CCTV Upgrades</a>
                    </li>
                    <li>
                        <a href="http://www.avsecurity.com/bug-sweeping/">Bug Sweeping</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid footer-top">
            <div class="btn-group">
                <button style="z-index:100;" data-toggle="modal" data-target="#contact-modal" class="btn btn-primary btn-lg get-quote-btn btn-fab"
                    id="get-a-quote-now">GET A QUOTE NOW<i class="quote-chevron fa fa-chevron-right"></i></button>
            </div>
</div>
<footer class="dark-gradient">
    <?php wp_footer(); ?>
    <a id="back-to-top" href="#" class="btn btn-default btn-lg back-to-top" role="button" title="Click to return on the top page"
        data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
    <div id="footer">
        <img class="hidden-xs" id="footer-overlay" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-250.png"
            alt="AV Security">
        <img class="visible-xs" id="footer-overlay-sm" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-250.png"
            alt="AV Security">
        <!-- largefooter -->
        <div class="container">
            <div class="row hidden-xs">
                <div class="col-md-6 col-xs-6 lead" id="getintouch" style="text-decoration: none;">
                    <h2>Get in Touch</h2>
                    <p><i id="footer-email-icon" class="fa fa-envelope"></i> Email <a id="large-footer-email-link" title="email AV Security" href="mailto:info@avsecurity.com?Subject=Enquiry"
                            target="_blank">info@avsecurity.com</a></p>
                    <p><i id="footer-phone-icon" class="fa fa-phone"></i>Call <a id="footer-call-link" href="tel:0035312870055">+353 (0)1 287 0055</a></p>
                    <!-- <a href="<?php bloginfo('url');?>/wp-content/uploads/2013/06/isiaLogo.png"><img class=" wp-image-558 alignright" alt="ISIA Irish Security Industry Association" src="<?php bloginfo('url');?>/wp-content/uploads/2013/06/isiaLogo.png" width="132" height="70" /></a>   -->
                    <div class="footersociallinks" id="sociallinkspanel">
                        <a class="socialicons" href="https://www.facebook.com/pages/Audio-Visual-Security-Ltd/1383275221917253" target="_blank" rel=noopener id="lg-footer-facebook-link">
                            <p><i id="footer-facebook-icon" class="fa-lg fa-facebook fa"></i> Facebook</p>
                        </a>
                        <a class="socialicons" target="_blank" rel=noopener id="lg-footer-facebook-link" href="http://ie.linkedin.com/pub/alan-campbell/26/92/699">
                            <p><i id="footer-linkedin-icon" class="fa-lg fa-linkedin fa"></i> LinkedIn</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-6" style="padding-top:60px;">
                    <a id="psa-link" target="_blank" rel=noopener id="footer-psa-link" href="http://www.psa.gov.ie/"><img class="licenses img-rounded pull-right" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/private-security-authority1.png"/></a>
                </div>
                <div class="col-md-6">
                    <a id="footer-isia-link" target="_blank" rel=noopener href="http://www.isia.ie/av-security"><img id="nomarginlicence" class="licenses img-rounded pull-right" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-industry.png"/></a>
                </div>
            </div>
            <!-- Small Footer -->
            <div class="row visible-xs">
                <div class="col-xs-12 lead" id="getintouch">
                    <h2>Get in Touch</h2>
                    <p>
                        <i id="footer-email-icon-xs" class="fa fa-envelope"></i> Email <a title="Email AV Security" id="sm-footer-email-link" href="mailto:info@avsecurity.com?Subject=Enquiry">info@avsecurity.com</a></p>
                    <a href="tel:0035312870055">
                        <p><i id="footer-phone-icon-xs" class="fa fa-phone"></i> Call <a id="sm-footer-call-link">+353 (0) 1 287 0055</a></p>
                    </a>
                    <div class="footersociallinks" id="sociallinkspanel">
                        <a class="socialicons" target="_blank" rel=noopener id="facebook-page-link"  href="https://www.facebook.com/pages/Audio-Visual-Security-Ltd/1383275221917253">
                            <p><i class="fa-lg fa-facebook fa" id="footer-facebook-icon-xs"></i>Facebook</p>
                        </a>
                        <a class="socialicons" target="_blank" rel=noopener id="linkedin-link" href="http://ie.linkedin.com/pub/alan-campbell/26/92/699">
                            <p><i class="fa-lg fa-linkedin fa" id="footer-linkedin-icon-xs"></i>LinkedIn</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="focalise-link text-center">
                    <i class="fa fa-cloud"></i> Website by <a target="_blank" rel=noopener href="https://focalise.ie" id="footer-focalise-link">Focalise.ie</a>
                </div>
            </div>
        </div>
    </div>
    <div class="rss-feeds">
        <?php bloginfo('atom_url'); ?>
        <?php bloginfo('rss2_url'); ?>
    </div>
</footer>
<div id="contact-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Contact Us</h4>
            </div>
            <div class="modal-body">
                <p class="lead">If you would like to make an enquiry about any of our services or products, we would be delighted to hear
                    from you.</p>
                    <form style="margin-top: 2em;" action="https://formspree.io/info@avsecurity.com"
      method="POST">
<label>Name</label>
    <input class="form-control" type="text" name="name">
<label>Phone Number</label>
    <input class="form-control" type="text" name="number">
<label>Email</label>    
<input class="form-control" type="email" name="_replyto">
<label>Your Enquiry</label>    
<input class="form-control" type="textarea" name="Your Enquiry">
    <input class="btn btn-primary" type="submit" value="Send">
</form>
  
                <p class="lead">You can call <a href="tel:0035312870055"><strong>01 287 0055</strong></a> or email <a href="mailto:info@avsecurity.com?Subject=Enquiry"><strong>info@avsecurity.com</strong></a></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<link lazyload href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("img", this).addClass("img-responsive");
        jQuery('#back-to-top').fadeOut('fast');
        jQuery('#back-to-top').tooltip('hide');
        jQuery('#get-a-quote-now').fadeOut('fast');
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 700) {
                jQuery('#get-a-quote-now').fadeIn('fast');
                jQuery('#back-to-top').fadeIn('fast');
            } else {
                jQuery('#get-a-quote-now').fadeOut('fast');

                jQuery('#back-to-top').fadeOut('fast');
                jQuery('#back-to-top').tooltip('hide');
            }
        });
        jQuery('#back-to-top').click(function () {
            jQuery('#back-to-top').tooltip('hide');
            jQuery('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });
</script>
</body>
</html>