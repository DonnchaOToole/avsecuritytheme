<?php get_header(); ?>
<div class="jumbotron about-hero dark-gradient" id="whoarewejumbo">
    <div class="avfrontsliderpanel container slideInLeft">
        <div class="row">
            <div class="col-md-2">
                <img id="navbarlogo" class="img-responsive hidden-sm hidden-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png">
            </div>
            <div class="col-md-10">
                <h1 class="bold">AV Security</h1>
                <p class="text-white">Helping prevent fraud and breaches of security for businesses and homes.</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <p class="lead">AV Security was set up in 1996 and is licensed by the Private Security Authority (PSA) to help companies and
                private individuals to ring-fence the safety of their communications, commercial transactions and physical
                security. </p>
        </div>
        </div>
        <div class="row">
             <div class="col-sm-4 visible-xs">
            <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/21-years.jpg" alt="" class="img-responsive">
            </div>
            <div class="col-sm-8">
            <h2>21 Years of Experience</h2>
            <p>We have worked in the corporate arena with multinational and private companies to establish safety procedures
                which ensure privacy practices which are second to none. In this time, the huge technology advances we have
                all experienced through access to the internet have also opened greater possibilities for breaches of privacy.
                AV Security has effectively matched the perceived risks with countermeasures to bring about security solutions.
                We pride ourselves on working closely with our clients to firstly develop a core understanding of the business
                and where security risks apply.</p>
            
            
            </div>
            <div class="col-sm-4 hidden-xs">
            <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/21-years.jpg" alt="" class="img-responsive">
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8">
            <h2>First Class Security</h2>
            <p>Coupled with in-depth analysis of your business model, we provide advice and the necessary infrastructure to
                ensure that business owners and facilities managers are delivered first class security systems aimed at deterring,
                recording and averting real-time risks. Our service is personal and friendly and we are on-hand at all times
                post installation, to answer queries or respond to concerns or escalations to security risks.</p>
            
            </div>
        </div>

    <section>

    <div class="row">
        <div class="col-sm-4">
            <div class="alan-img-wrap">

            <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/alan-avsecurity.jpg" alt="Alan Campbell. Managing Director of AVSecurity" class="alan-photo img-responsive">
            <p>Alan Campbell, Managing Director</p>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="panel panel-default" id="delightedtotakeyourcall">
                <div class="row">
                    <div class="col-sm-3"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/AVS-Logo.png" alt="avsecurity" class="img-responsive">

                    </div>
                    <div class="col-md-9 col-sm-9">
                        <p class="lead">We understand that your security is key to the smooth running of your business or lifestyle and our
                            aim is to provide a first-class and professional security service.
                        </p>
                        <p class="lead">I would be delighted to discuss your particular needs in full confidence and with no obligation and
                            can be reached
                            <br/>on <a href="tel:00353872528777"><strong>087 2528 777</strong></a>.
                        </p>
                        <p class="lead">I look forward to taking your call.
                        </p>
                        <p class="lead">Alan Campbell</p>
                        <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/as.png" alt="security ireland">
                    </div>
                </div>
            </div>

        </div>
    </div>

    </section>
</div>

</div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>Testimonials <small>What our customers are saying</small></h1>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <img src="https://www.avsecurity.com/wp-content/uploads/2017/03/boeing-logo.png" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8">
                    <blockquote>
                        <p>I was at the new office in Dublin last week and just wanted to let you know that the work you have
                            done is excellent – really good finishing on the doors and the CCTV/access control has been installed
                            really well. It has been refreshing to work with Alan and his team on this project. Thanks for
                            your great work.</p>
                        <footer>Alykhan Tejani - Real Estate Project Manager for Boeing Aerospace</footer>
                    </blockquote>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <img src="https://www.avsecurity.com/wp-content/uploads/2017/03/cooneycarey-logo-new.png" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8">
                    <blockquote class="blockquote">We have used the services of Alan Campbell and his company Audio Visual Security Ltd (AVS) on a number
                        of fraud investigations. His attention to detail and the results of his investigation work was an
                        essential part of investigations. We would have no hesitation in recommending his services. The types
                        of work that AVS provided was:
                        <ul>
                            <li>Surveillence</li>
                            <li>Covert CCTV</li>
                            <li>Interviewing of witnesses</li>
                            <li>Wi-Fi Audit </li>
                        </ul>
                        <footer>Paul Leonard, <a target="_blank" rel=noopener id="cooney-carey-link" href="http://www.cooneycarey.ie/">Cooney Carey Chartered Accountants</a>, Taxation
                            & Business Advisors
                        </footer>
                    </blockquote>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <img src="https://www.avsecurity.com/wp-content/uploads/2017/03/logo.png" alt="" class="img-responsive">
                </div>
                <div class="col-sm-8">
                    <blockquote>
                        <p>“We have a service contract with AV security and have a long experience using their services. I found
                            AVS prompt and professional.</p>
                        <p>I also appreciated the effort and time spent demonstrating the equipment and how it detects.”</p>
                        <footer>Des Mulcair - <a target="_blank" rel=noopener id="roadbridge-link" href="https://www.roadbridge.ie/">Roadbridge</a>, Civil Engineering and Building
                            Contractors, Ireland
                        </footer>
                    </blockquote>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-8">
                    <blockquote>
                        “A highly professional service that leaves me with a comfortable feeling about the privacy of meetings in our boardroom.”
                        <footer>Security Director, Health Care Products Manufacturer</footer>
                    </blockquote>

                </div>
            </div>

            <!-- <blockquote>“We have a service contract with AV security and have a long experience using their services. I found AVS prompt and professional. I also appreciated the effort and time spent demonstrating the equipment and how it detects.”


			<footer>Financial Director, National Newspaper</footer></blockquote> -->


            <div class="row">
                <div class="col-sm-4">

                </div>
                <div class="col-sm-8">
                    <blockquote>“We took the decision to inspect our premises for listening devices based on high risk and low probability.
                        We looked for a company who had the following qualities: standards, previous customers recommendations
                        and qualifications. AV Security are our chosen provider. We would recommend them to anyone requiring
                        specialist services.”
                        <footer>Head Of Security, Financial Institution</footer>
                    </blockquote>
                </div>
            </div>


        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-6" id="centered-button">
            <a class="btn btn-lg btn-primary" href="
				<?php
				$blog_id = get_current_blog_id();
				echo get_home_url( $blog_id, 'contact-us' ); ?>/">Get in touch <i style="color:white;" class="fa fa-lg fa-arrow-right"></i></a>
        </div>
    </div>
</div>
<?php get_footer(); ?>