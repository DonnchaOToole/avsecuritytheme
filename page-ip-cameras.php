<?php get_header(); ?>
<div id="ip-cameras-header" class="jumbotron dark-gradient">
    <div class="container animated slideInLeft">
        <h1>High Resolution Cameras</h1>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/ir-camera.jpg" class="img-responsive"></div>
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-dome.jpg" class="img-responsive"></div>
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-bullet.jpg" class="img-responsive"></div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h2>High-Resolution Security Camera Systems</h2>
            <p class="lead">The fact is, the clearer the picture, the better.</p>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <h2>Hybrid Systems</h2>
            <p>If your Business has an existing analogue CCTV system, AV Security can install a hybrid digital recorder that
                will continue to record your analogue cameras with the ability to add a number of IP cameras.</p>
            <p>A professionally installed IP camera system will deliver a higher definition image compared to an analogue system.
                This can be particularly beneficial in certain circumstances.</p>
            <ul class="list-group">
                <li class="list-group-item">Identification of money notes.</li>
                <li class="list-group-item">Car registration plates.</li>
                <li class="list-group-item">Fewer cameras required.</li>
            </ul>
        </div>
    </div>
</div>
        <?php require_once('remote-viewing.php'); ?>

<?php get_footer(); ?>