<?php get_header();?>
<div class="container" style="padding-top: 4rem; padding-bottom: 4rem;">
<h1>404</h1>
<h2>We can't find the page you are looking for.</h2>
<a href="https://www.avsecurity.com" class="btn btn-default">Return to home page</a>
</div>
<?php get_footer();?>