<?php get_header(); ?>
<div class="jumbotron" id="electronicbugsweeping">
	<div class="container animated slideInLeft">
	<h1 class="text-white">The History of Listening Devices</h1>
	<p class="text-white">Think there might be someone listening in?</p>
	<a class="btn btn-primary btn-lg" href="<?php
                $blog_id = get_current_blog_id();
                echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button">Get in Touch »</a>
	</div>
</div>









<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h4>The Thing <small> The "Great Seal" bug</small></h4>
            <p>
                <p>The Thing, also known as the Great Seal bug, was one of the first covert listening devices (or "bugs") to use passive techniques to transmit an audio signal.</p>
<p>The Thing was designed by Léon Theremin, who also made the eponymous theremin. It consisted of a tiny capacitive membrane connected to a small quarter-wavelength antenna; it had no power supply or active electronic components. The device, a passive cavity resonator, became active only when a radio signal of the correct frequency was sent to the device from an external transmitter. Sound waves caused the membrane to vibrate, whch varied the capacitance "seen" by the antenna, which in turn modulated the radio waves that struck and were retransmitted by the Thing. A receiver demodulated the signal so that sound picked up by the microphone could be heard, just as an ordinary radio receiver demodulates radio signals and outputs sound.</p>
<p>Theremin's design made the listening device very difficult to detect, because it was very small, had no power supply or active components, and did not radiate any signal unless it was actively being irradiated remotely. These same design features, along with the overall simplicity of the device, made it very reliable and gave it a potentially unlimited operational life.</p>            
            
        </div><!--.col -->
        
        <div class="col-md-8">
            <div class="vid 2emheadspace">
                <iframe width="640" height="360" src="https://www.youtube-nocookie.com/embed/YPJjxiuyy4A?rel=0" allowfullscreen="true"></iframe>
            </div><!--./vid -->
            
        </div><!--.col -->
        
    </div><!--./row -->
    
</div><!--./container -->


<?php get_footer(); ?>: