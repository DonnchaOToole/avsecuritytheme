<div class="container remote-viewing">
    <div class="row">
        <div class="hidden-xs col-sm-5">
            <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/03/thief-phone.jpg">
        </div>
        <div class="col-xs-12 col-sm-5">
        <h2>Remote Monitoring</h2>
        <p class="lead">Monitor your CCTV system on your iPhone, Android, tablet, laptop or PC.</p>
        <button class="btn btn-primary btn-lg mb-2" data-toggle="modal" data-target="#contact-modal">Get a quote now</button>
        </div>
        <div class="col-xs-12 visible-xs">
        <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/03/thief-phone.jpg">
        </div>
    </div>
</div>