<?php
/*
Template Name: CCTV Page Template
Designer: http://focalisewebdesign.com
*/

get_header(); ?>
			
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>


		<?php endwhile; else: ?>
	  		<p><?php _e('Sorry, this page does not exist.'); ?></p>
		<?php endif; ?>

<div class="container">
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="col-md-6">
		   		<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/image001.jpg" class="img-responsive">
		   	</div>
			
			<div class="col-md-6 text-center 1emheadspace">
				<a href="
				<?php
				$blog_id = get_current_blog_id();
				echo get_home_url( $blog_id, 'contact-us' ); ?>
				" role="button">
			   		<p class="lead vertical-align">Get a free quote for <br/>CCTV installation</p>
			   		<p>Peace of mind is only a few clicks away.</p>
			   		<a class="btn btn-default btn-lg">Get your free quote</a>
			   	</a>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
    