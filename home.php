<?php get_header(); ?>
    <div class="jumbotron newspaper-bg" id="whoarewejumbo">
        <div class="avfrontsliderpanel container animated slideInLeft">
            <div class="row">
                <div class="col-md-2">
                    <img class="img-responsive hidden-sm hidden-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png">
                </div>
                <div class="col-md-10">
                    <h1 class="bold">News</h1>
                    <p class="text-white">What's happening in the security industry.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            query_posts('cat=13&posts_per_page='.get_option('posts_per_page').'&paged=' . $paged);
            ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <!-- WRAPS -->
                        <a href="<?php the_permalink(); ?>">
                            <div class="media news-item">
                                <a class="pull-left" href="<?php the_permalink();?>">
                                    <?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail');} ?>
                                </a>
                                <div class="media-body">
                                    <h3 class="news-article-summary-headers media-heading">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <em><p><?php the_date();?></p></em>
                                    <p>
                                        <?php the_excerpt();?>
                                    </p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-lg btn-default pull-right">Read More</a>
                                </div>
                            </div>
                        </a>
                        <?php endwhile; ?>
                            <?php
                if ( function_exists('vb_pagination') ) {
                 vb_pagination();
                        }
            ?>
                                <?php else : ?>
                                    <p>
                                        <?php _e('Sorry, there are no posts.'); ?>
                                    </p>
                                    <?php endif; ?>
                                        <!-- <div class="col-md-4">
        <?php get_sidebar(); ?> 
        </div> -->
            </div>
            <div class="col-md-4 hidden-xs">
                <a href="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Corporate-investigations-threat-analysis.pdf" class="btn btn-primary btn-lg"><i class="fa fa-lg fa-arrow-down" style="color:white;"></i> Threat analysis brochure</a>
                <a href="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Technical-Surveillance.pdf" class="btn btn-primary btn-lg"><i class="fa fa-lg fa-arrow-down" style="color:white;"></i> TSCM brochure</a>
                <h3>Recent News:</h3>
                <ul class="list-group">
                    <?php

                $args = array( 'posts_per_page' => 15, 'offset'=> 0, 'category' => 13 );

                $myposts = get_posts( $args );
                foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
                        <li class="list-group-item">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </li>
                        <?php endforeach; 
                wp_reset_postdata();?>
                </ul>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
