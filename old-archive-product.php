<?php get_header(); ?>
    <div class="jumbotron bluegradient" id="products">
        <div class="container animated slideInLeft">
            <div class="col-xs-2">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-150.png" alt="AV Security" id="avs-slider-slogo">
            </div>
            <div class="col-xs-8">
                <h1 id="security-products-header-text">Security Products</h1>
                <a class="btn btn-default btn-lg" href="<?php	
					$blog_id = get_current_blog_id();
					echo get_home_url( $blog_id, 'contact-us' ); ?>" role="button">Get in Touch »</a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8 product-page-wrap">
                <?php query_posts('post_type=product'); ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <div class="row panel">
                            <div class="col-md-3 col-sm-3">
                                <?php if ( has_post_thumbnail() ) { the_post_thumbnail('thumbnail'); } ?>
                            </div>
                            <div class="col-md-9 col-sm-9">
                                <h2><a style="text-decoration: none;" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <p>
                                    <?php if ( has_excerpt() ) { the_excerpt(); } ?>
                                </p>
                                <a href="<?php the_permalink(); ?>" class="btn btn-lg btn-default">More info</a>
                                <!-- <a href="<?php
							$blog_id = get_current_blog_id();
							echo get_home_url( $blog_id, 'contact-us' ); ?>" 
							class="btn btn-lg btn-default">Price Request</a> -->
                                <hr/>
                            </div>
                        </div>
                        <?php endwhile; else: ?>
                            <p>
                                <?php _e('Sorry, there are no products.'); ?>
                            </p>
                            <?php endif; ?>
            </div>
            <!-- <div class="col-md-4">
		<?php get_sidebar(); ?>	
		</div> -->
        </div>
    </div>
    <?php get_footer(); ?>
        <script>
        jQuery("img").addClass("img-rounded");
        </script>
