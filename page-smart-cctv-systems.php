<?php get_header(); ?>
<div class="jumbotron smart-cctv">
    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                <h1>Smart CCTV</h1>
                <h3>Turning video into intelligence</h3>
            </div>
            <div class="col-xs-4">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/brain.png" alt="" class="img-responsive">
            </div>
        </div>

    </div>
</div>
<div class="container">
    <section class="smart-cctv">
        <div class="row smart-features">
            <div class="col-sm-6">
                <h2>Smart CCTV</h2>
                <p class="lead">
                    AV Security is a leader in intelligent IP video surveillance solutions. We help organizations realize the true power of integrated
                    data and video.
                </p>
                <p>Our automated intelligent CCTV surveillance, operating with advanced remote CCTV monitoring technologies
                    results in the best of both worlds, where intelligent CCTV surveillance automation interplays with human
                    vigilance. This new partnership optimises the key human element of security by not depending on human
                    intervention to detect and capture evidence of crime, and index its evidence. This means that operators
                    have more time to react in making the key decisions for responding.</p>
            </div>
            <div class="col-sm-6">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/smart-cctv.jpg" alt="" class="img-responsive">
            </div>
        </div>
    </section>
    <section>
        <div class="row smart-features">
            <div class="col-sm-4">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/anpr.jpg" alt="">
            </div>
            <div class="col-sm-8">
                <h3>ANPR</h3>
                <p>On-board ANPR analytics allow the ability to detect and recognize a vehicle's license plate and send ANPR
                    info to a Smart NVR or iVMS-5200P for access management. Supports may vary based on local LPR algorithm
                    development and customization process required.</p>
            </div>
        </div>
        <div class="row smart-features">
            <div class="col-sm-4">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/cctv-area.jpg" alt="">
            </div>
            <div class="col-sm-8">
                <h3>Region Enter / Exit</h3>
                <p>This function detects people, vehicles or other objects that enter or exit from a pre-defined virtual region,
                    triggering an alarm.</p>
            </div>
        </div>
        <div class="row smart-features">
            <div class="col-sm-4">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/office-cctv.jpg" alt="">
            </div>
            <div class="col-sm-8">
                <h3>Object Removed / Object Left Behind</h3>
                <p>This function detects objects left or removed from a pre-defined region, the alarm is triggered once the
                    object is removed or placed within this region as per the schedule.</p>
            </div>
        </div>
        <div class="row smart-features">
            <div class="col-sm-4">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/05/cctv-zones.jpg" alt="">
            </div>
            <div class="col-sm-8">
                <h3>Object Counting</h3>
                <p>You can designate an area of interest and let the camera count objects entering or leaving, which include
                    not only humans but also vehicles. It also offers statistical reports.</p>
            </div>
        </div>
    </section>

</div>
<?php require_once('remote-viewing.php'); ?>
<?php require_once('price-match-cta.php'); ?>
<?php get_footer(); ?>