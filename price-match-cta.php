<div class="price-match-wrap">
<img src="https://www.avsecurity.com/wp-content/uploads/2017/05/price-match-4.jpg" alt="" class=" price-match-img img-responsive">
</div>
<div class="call-now-wrap">
    <div class="container">
        <div class="col-sm-6">
        <h4 style="margin-top: 3rem;">Call now for a free quote: + 353 1 287 0055</h4> 
        </div>
        <div class="col-sm-6">
        <button style="margin-bottom: 1.75rem;" data-toggle="modal" data-target="#contact-modal" class="btn btn-primary btn-lg btn-fab"
                >Send us a message<i class="quote-chevron fa fa-chevron-right"></i></button>
        
        </div>
    </div>
</div>