<?php get_header(); ?>
<div id="ip-cameras-header" class="jumbotron dark-gradient">
    <div class="container animated slideInLeft">
        <h1>CCTV Upgrades</h1>
    </div>
</div>
<section class="upgrades-sub-header">
    <div class="container">
        <h2>Upgrading or Replacing your Old Analogue CCTV System</h2>
    </div>
</section>
<div class="container">
    <!--<div class="row">
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/ir-camera.jpg" class="img-responsive"></div>
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-dome.jpg" class="img-responsive"></div>
        <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-bullet.jpg" class="img-responsive"></div>
    </div>-->
    <section>
        <div class="row">
            <div class="col-sm-6 hidden-xs">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/04/old-cctv.jpg" class="img-responsive" style="margin-bottom: 8rem; opacity: 0.4;" alt="">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/04/upgraded-camera.jpg" class="img-responsive">
                 
            </div>
            <div class="col-sm-6">
                <h2>Upgrade my current system</h2>
                <p class="lead">Do you have an existing CCTV system that you want to upgrade to high definition CCTV?</p>

                <p>90% of current CCTV systems use coaxial cable system at present and can be upgraded to HD quality.</p>

                <p>It is now possible to upgrade your CCTV security system to HD quality without rewiring. AV Security HD CCTV
                solution is perfect for home or businesses.</p>
                <p class="hidden-xs">If your system already has coax cabling you simply need to switch out your video recorder and replace the
                    existing cameras. Now you will have a high definition (1080p) surveillance system.</p>
            </div>
        </div>
        <div class="row visible-xs">
            <div class="col-xs-6">
                <p>If your system already has coax cabling you simply need to switch out your video recorder and replace the
                    existing cameras. Now you will have a high definition (1080p) surveillance system.</p>
            </div>
            <div class="col-xs-6">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/04/upgraded-camera.jpg" class="img-responsive">
            </div>
        </div>
    
</section>

<section>
    <div class="row">
        <div class="col-sm-6">
       
            <img src="https://www.avsecurity.com/wp-content/uploads/2017/04/video-recorder.jpg" class="img-responsive" alt="">
        </div>
        <div class="col-sm-6">
            <h2>Keep some of the CCTV cameras from my current system</h2>
            <p>Our Hikvision video recorders are compatible with analogue CCTV cameras allowing you to choose which cameras
                you want to change first, so you don&#8217;t necessarily have to change all your cameras at once. Change
                your video recorder and either replace all your cameras from our HD Turbo range, keep some of your old cameras
                and fit HD Turbo cameras in locations where you need high definition images. The video recorder will automatically
                detect the type of camera that is connected and adjusts recording resolution to suit.</p>
        </div>
    </div>
</section>
<!--<div class="text-center"><button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#contact-modal">Get a quote</button></div>-->
</div>
<?php require_once('remote-viewing.php'); ?>

<?php get_footer(); ?>