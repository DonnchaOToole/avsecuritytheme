<?php
/*
Template Name: Splash
Designer: http://focalisewebdesign.com
*/
?>
    <?php get_header(); ?>
        <div id="myCarousel" class="carousel slide" data-ride="carousel" data-wrap="true">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active">
                    <img class="hidden-xs img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/avsecurity-header.jpg" alt="First slide">
                    <img class="visible-xs img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/avsecurity-header-xs.jpg" alt="First slide">
                    <div class="container">
                        <div class="carousel-caption">
                            <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png" alt="AV Security" id="avs-slider-slogo">
                            <h1>AV Security</h1>
                            <p>Preventing fraud and breaches of security for businesses and homes for the past 20 years.</p>
                            <p><a class="btn btn-lg btn-default" href="#" role="button">Get in touch today</a></p>
                        </div>
                    </div>
                </div>
                <!-- COMMERCIAL CCTV -->
                <div class="item">
                    <img class="visible-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-business-xs.jpg" alt="Second slide">
                    <img class="hidden-xs hidden-sm" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-business.jpg" alt="Second slide">
                    <img class="visible-sm" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-business-sm.jpg" alt="Second slide">
                    <div class="container">
                        <div class="carousel-caption hidden-xs">
                            <h1><i style="color:white;" class="fa fa-4x fa-video-camera"></i></h1>
                            <h1>Commercial CCTV</h1>
                            <p>Regardless of the type of premises you are looking to protect
                                <br/> AV Security has the solution</p>
                            <p><a class="btn btn-lg btn-default" href="#" role="button">More about our CCTV systems</a></p>
                        </div>
                        <div class="carousel-caption visible-xs">
                            <h2><i style="color:white;" class="fa fa-3x fa-video-camera"></i></h2>
                            <h2>Commercial CCTV</h2>
                            <p>Regardless of the type of premises you are looking to protect
                                <br/> AV Security has the solution</p>
                            <p><a class="btn btn-lg btn-default" href="#" role="button">More about our CCTV systems</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugsweeping-xs1.jpg" class="img-responsive visible-xs" alt="Bug Sweeping">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugsweeping-lg1.jpg" class="img-responsive hidden-sm hidden-xs" alt="Bug Sweeping">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugsweeping-sm.jpg" class="img-responsive visible-sm" alt="Bug Sweeping">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1><i id="carousel-microphone" class="fa fa-4x fa-microphone-slash"></i></h1>
                            <h1>Bug Sweeping</h1>
                            <p id="bugsweeping-carousel-p">If you think you are at risk from electronic eavesdropping, we can help.</p>
                            <p><a class="btn btn-lg btn-default" href="#" role="button">Learn more</a></p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img class="hidden-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-homes.jpg" alt="Home  CCTV">
                    <img class="visible-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-homes-xs.jpg" alt="Home CCTV">
                    <div class="container">
                        <div class="carousel-caption" id="home-cctv-carousel">
                            <h1><i style="color:white;" class="fa fa-4x fa-video-camera"></i></h1>
                            <h1>Home CCTV</h1>
                            <p>Protect your family and your property</p>
                            <p><a class="btn btn-lg btn-default" href="#" role="button">Learn more</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
        <!-- /.carousel -->
        <section id="front-page-marketing-row-of-3">
            <div class="container marketing">
                <div id="frontPageRow3" class="row animated slideInRight text-left">
                    <div class="col-md-4 col-sm-4 marketingdivs"><i class="fa fa-video-camera fa-5x"></i>
                        <h2>CCTV</h2>
                        <p>With extensive experience in Fraud Detection, AV Security provides a personalised, discreet and highly effective service with installation of CCTV Electronic Surveillance, tailored to suit your individual needs.</p>
                        <a class="marketingbuttons btn btn-primary" role="button" href="<?php bloginfo('url');?>/cctv">Learn more »</a>
                    </div>
                    <div class="col-md-4 col-sm-4 marketingdivs"><i class="fa fa-question fa-5x"></i>
                        <h2>Investigations</h2>
                        <p>We can investigate either covertly or openly. We can help you with: employee infidelity, insurance fraud, IT security, due dilligence, protecting intellectual property and screening of new employees.</p>
                        <a class="marketingbuttons btn btn-primary" role="button" href=" <?php bloginfo('url');?>/business-security/">Learn more »</a>
                    </div>
                    <div class="col-md-4 col-sm-4 marketingdivs"><i class="fa fa-microphone-slash fa-5x"></i>
                        <h2>Bug Sweeping</h2>
                        <p>Whether you and your senior management team are discussing potential mergers and acquisitions, company policy, marketing plans or new product design, you are at risk from the professional buggist.</p>
                        <a class="marketingbuttons btn btn-primary" role="button" href="<?php bloginfo('url');?>/bug-sweeping/">Learn more »</a>
                    </div>
                </div>
            </div>
        </section>
        <hr>
        <div class="container marketing">
            <div id="securitysidetoside" class="row sidetosidepanels animated fadeInUp">
                <div class="col-md-4 col-sm-4">
                    <img class="img-responsive img-rounded" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/iStock_000003504050_ExtraSmall.jpg" />
                </div>
                <div class="col-md-8 col-sm-8">
                    <h2>Security for business</h2>
                    <p>We can investigate either covertly or openly. We can help you with: employee infidelity, insurance fraud, IT security, due dilligence, protecting intellectual property and screening of new employees.</p>
                    <a class="marketingbuttons btn btn-primary" role="button" href="#">Learn more »</a>
                </div>
            </div>
            <div class="row sidetosidepanels 2emheadspace">
                <div class="col-sm-4 col-md-4">
                    <img class="img-responsive img-rounded" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Photocall-9779-Housing-Estates-00137425200.jpg" />
                </div>
                <div class="col-md-8 col-sm-8">
                    <h2>Security at home</h2>
                    <p>We install of security camera systems internally and externally to provide you with peace of mind on a day to day basis when you cannot be at your property, or while you and your family are asleep.</p>
                    <a class="marketingbuttons btn btn-primary" role="button" href="#">Learn more »</a>
                </div>
            </div>
            <hr/>
            <div id="avsjumbotron" class="jumbotron 2emheadspace">
                <h2>Who are AV Security?</h2>
                <p>AV Security has over twenty years experience providing highly confidential security services to a range of Ireland’s public and commercial sector and overseas clients. We are an independent Irish company working to ensure your private and confidential information remains just that. We provide a very high degree of professional integrity in dealing with all our clients. We regard every client as unique, every situation as different and pride ourselves on responding uniquely to each client’s individual need.</p>
                <p><a href="<?php bloginfo('url');?>/why-choose-us/testimonials/" class="btn btn-primary btn-lg" role="button">Testimonials »</a><a class="btn btn-warning btn-lg spaceonleft" role="button">Get in touch »</a></p>
            </div>
        </div>
        <?php get_footer(); ?>
