<?php
/*
Template Name: AVSecurity
Designer: https://focalise.ie
*/
?>
    <?php get_header(); ?>
    <div id="avCarousel" class="carousel slide" data-ride="carousel" data-wrap="true">
        <ol class="carousel-indicators">
            <li data-target="#avCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#avCarousel" data-slide-to="1"></li>
            <li data-target="#avCarousel" data-slide-to="2"></li>
            <li data-target="#avCarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <img class="hidden-xs hidden-sm img-responsive" src="<?php bloginfo('url');?>
/wp-content/uploads/2014/05/avsecurity-header.jpg" alt="Photo of Beckett Bridge Dublin">
                <img class="visible-sm img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/avsecurity-front-sm2.jpg"
                    alt="Photo of Beckett Bridge Dublin">
                <img class="visible-xs img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/LiffeyDublin-xs.jpg" alt="Photo of Beckett Bridge Dublin">
                <div class="container">
                    <div class="carousel-caption hidden-xs">
                        <img class="center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-150.png" class="center-block"
                            alt="AV Security" id="avs-slider-slogo">
                        <h1 class="front-avs-hero-text">AV Security</h1>
                        <p>Preventing fraud and breaches of security for businesses and homes for 21 years.</p>
                        <p><a class="btn btn-lg btn-default" href="<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button">Get a free quote</a></p>
                    </div>
                    <div class="carousel-caption visible-xs iphone">
                        <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png" alt="AV Security" id="avs-slider-logo-xs"
                            class="center-block">
                        <h1>AV Security</h1>
                        <p>21 years preventing fraud and security breaches for irish businesses and homes.</p>
                        <p><a class="btn btn-lg btn-default" href="tel:0035312870055" role="button">Get in touch today</a></p>
                    </div>
                </div>
            </div>
            <!-- COMMERCIAL CCTV -->
            <div class="item">
                <img class="visible-xs" src="https://www.avsecurity.com/wp-content/uploads/2017/05/commercial-cctv-xs-1.jpg" alt="Commercial CCTV">
                <img class="hidden-xs hidden-sm" src="https://www.avsecurity.com/wp-content/uploads/2017/05/commercial-cctv.jpg" alt="Commercial CCTV">
                <img class="visible-sm" src="https://www.avsecurity.com/wp-content/uploads/2017/05/commercial-sm-cctv.jpg" alt="Commercial CCTV">
                <div class="container">
                    <div class="carousel-caption hidden-xs">
                        <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-150.png" class="center-block" alt="AV Security"
                            id="avs-slider-slogo">
                        <!-- <h1><i style="color:white;" class="fa fa-4x fa-video-camera"></i></h1>               -->
                        <h1>Commercial CCTV</h1>
                        <p>Who is keeping an eye on your business?</p>
                        <p><a class="btn btn-lg btn-default" href="<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'commercial-cctv' ); ?>/" role="button">CCTV Installations</a></p>
                    </div>
                    <div class="carousel-caption visible-xs iphone">
                        <!-- <h2><i style="color:white;" class="fa fa-3x fa-video-camera"></i></h2>               -->
                        <img class="center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png" alt="AV Security"
                            id="avs-slider-logo-xs">
                        <h1>Commercial CCTV</h1>
                        <p>Regardless of the type of premises you are looking to protect, AV Security has the solution.</p>
                        <p><a class="btn btn-lg btn-default" href="<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'commercial-cctv' ); ?>/" role="button" role="button">CCTV solutions</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bug-sweeping-xs2.jpg" class="visible-xs" alt="Bug Sweeping">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugsweeping-lg3.jpg" class="hidden-sm hidden-xs" alt="Bug Sweeping">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugsweeping-sm1.jpg" class="visible-sm" alt="Bug Sweeping">
                <!-- Bug Sweeping -->
                <div class="container">
                    <div class="carousel-caption hidden-xs">
                        <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-150.png" class="center-block" alt="AV Security"
                            id="avs-slider-slogo">
                        <h1>Bug Sweeping</h1>
                        <p>Your privacy is our main concern.</p>
                        <p><a class="btn btn-lg btn-default" href="<?php
              $blog_id = get_current_blog_id();
              echo get_home_url( $blog_id, 'bug-sweeping' ); ?>/" role="button" role="button">Learn more</a></p>
                    </div>
                    <div class="carousel-caption visible-xs iphone">
                        <img class="center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png" alt="AV Security"
                            id="avs-slider-logo-xs">
                        <h1>Bug Sweeping</h1>
                        <p>If you think you are at risk from electronic eavesdropping, we can help.</p>
                        <p><a class="btn btn-lg btn-default" href="<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'bug-sweeping' ); ?>/" role="button" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img class="hidden-xs hidden-sm" src="https://www.avsecurity.com/wp-content/uploads/2017/05/home-cctv-cam2.jpg" alt="Home  CCTV">
                <img class="visible-xs" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/security-for-homes-xs.jpg" alt="Home CCTV">
                <img class="visible-sm" src="https://www.avsecurity.com/wp-content/uploads/2017/05/home-cctv-cam2.jpg" alt="Home CCTV">
                <div class="container">
                    <div class="carousel-caption hidden-xs" id="home-cctv-carousel">
                        <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-150.png" class="center-block" alt="AV Security"
                            id="avs-slider-slogo">
                        <h1>Home CCTV</h1>
                        <p>Protect your family and your property</p>
                        <p><a class="btn btn-lg btn-default" href="
        <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'home-cctv' ); ?>/" role="button
              " role="button">Learn more</a></p>
                    </div>
                    <div class="carousel-caption visible-xs iphone" id="home-cctv-carousel">
                        <img class="center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Av-security-umbrella-500.png" alt="AV Security"
                            id="avs-slider-logo-xs">
                        <h1>Home CCTV</h1>
                        <p>Protect your family and your property</p>
                        <p><a class="btn btn-lg btn-default" href="
        <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'home-cctv' ); ?>/" role="button
              " role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#avCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#avCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    <!-- /.carousel -->
    <section id="front-page-marketing-row-of-3">
        <div class="container marketing">
            <div class="front-page-row-3 row slideInRight text-left">
                <div class="col-sm-6">
                    <h2>CCTV</h2>
                    <p class="lead">Professional CCTV Installations</p>
                    <p style="text-align: justify;">With extensive experience in fraud detection, AV Security provides a personalised, discreet and highly
                        effective service with installation of CCTV electronic surveillance systems, tailored to suit your
                        individual needs.</p>
                    <a class="marketingbuttons btn btn-primary footroom" role="button" href="
        <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'commercial-cctv' ); ?>/" role="button
      ">Learn more »</a>
                </div>
               
                <div class="col-sm-6">
                    <h2>Bug Sweeping</h2>
                    <p class="lead">Protecting your privacy</p>

                    <p style="text-align: justify;">Whether you and your senior management team are discussing potential mergers and acquisitions, company
                        policy, marketing plans or new product design, you are at risk from the professional buggist.</p>
                    <a class="marketingbuttons btn btn-primary footroom" role="button" href="
    <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'bug-sweeping' ); ?>//" role="button
        ">Learn more »</a>
                </div>
            </div>
        </div>
    </section>
    <div class="container marketing">
        <div class="row">
            <div class="col-sm-6">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/2013101013520957.jpg" alt="">
            </div>
            <div class="col-sm-6">
                <h2 class "text-uppercase">Modern CCTV Systems</h2>
                <p class="lead">We supply and install a wide range of CCTV cameras for domestic, commercial and industrial purposes.</p>
            </div>
        </div>
    </div>
    <div id="business-security">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h2 class="text-uppercase">Business Security</h2>
                    <p class="lead">Protecting your business.</p>
                    <div class="btn-group btn-group-lg" role="group">
                        <button type="button" class="btn mb-2 btn-default">CCTV Installation</button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div style="margin-top: 2rem;" class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="//www.youtube.com/embed/0nOF3W8TvME"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row sidetosidepanels 2emheadspace">
            <div class="col-sm-4 col-md-4">
                <img class="img-responsive img-rounded" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Photocall-9779-Housing-Estates-00137425200.jpg"
                />
            </div>
            <div class="col-md-8 col-sm-8">
                <h2>Home Security</h2>
                <p class="lead">We carry out CCTV installation of security camera systems internally and externally to provide you with peace
                    of mind on a day to day basis when you cannot be at your property, or while you and your family are asleep.</p>
                <a class="marketingbuttons btn btn-lg btn-primary" role="button" href="
      <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'home-cctv' ); ?>/" role="button">Home CCTV Systems »</a>
            </div>
        </div>
        <div id="avsjumbotron" class="jumbotron 2emheadspace hidden-xs">
            <h2>Who are AV Security?</h2>
            <p>AV Security has over twenty years experience providing highly confidential security services to a range of Ireland’s
                public and commercial sector and overseas clients.</p>
            <p>We are an independent Irish company working to ensure your private and confidential information remains just
                that. We provide a very high degree of professional integrity in dealing with all our clients.</p>
            <p>We regard every client as unique, every situation as different and pride ourselves on responding uniquely to
                each client’s individual need.</p>
            <a href="
<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'about' ); ?>/" role="button
    " class="btn btn-primary btn-lg" role="button">Testimonials</a>
            <a class="hidden-xs btn btn-primary pull-right btn-lg" role="button" href="
  <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button
    ">Contact Us</a>
            <a class="visible-xs btn btn-success pull-right btn-lg" role="button" href="tel:0035312870055" role="button
        "><i class="fa fa-lg fa-phone" style="color:white;"></i></a>
        </div>
        <div class="well 2emheadspace visible-xs">
            <h2>Who are AV Security?</h2>
            <p>AV Security has over twenty years experience providing highly confidential security services to a range of Ireland’s
                public and commercial sector and overseas clients.</p>
            <p>We are an independent Irish company working to ensure your private and confidential information remains just
                that. We provide a very high degree of professional integrity in dealing with all our clients.</p>
            <p>We regard every client as unique, every situation as different and pride ourselves on responding uniquely to
                each client’s individual need.</p>
            <a href="
<?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'about' ); ?>/" role="button
    " class="btn btn-primary btn-lg" role="button">Testimonials »</a>
            <a class="hidden-xs btn btn-warning pull-right btn-lg" role="button" href="
  <?php
        $blog_id = get_current_blog_id();
        echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button
    ">Get in touch »</a>
            <a class="visible-xs btn btn-success pull-right btn-lg" role="button" href="tel:0035312870055" role="button
        "><i class="fa fa-lg fa-phone" style="color:white;"></i></a>
        </div>
    </div>
        <?php require_once('remote-viewing.php'); ?>

    <?php get_footer(); ?>