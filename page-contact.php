<?php get_header(); ?>
<div class="jumbotron about-hero dark-gradient" id="whoarewejumbo">
    <div class="avfrontsliderpanel container slideInLeft">
        
                <h1 class="bold">Contact</h1>
        
    </div>
</div>
<div style="min-height: 600px;" class="container">
<form style="margin-top: 2em;" action="https://formspree.io/info@avsecurity.com"
      method="POST">
<label>Name</label>
    <input class="form-control" type="text" name="name">
<label>Phone Number</label>
    <input class="form-control" type="text" name="number">
<label>Email</label>    
<input class="form-control" type="email" name="_replyto">
<label>Your Enquiry</label>    
<input class="form-control" type="textarea" name="Your Enquiry">
    <input class="btn btn-primary" type="submit" value="Send">
</form>
  
</div>
  <?php get_footer(); ?>