<?php
/*
Template Name: Commercial CCTV Page Template
Designer: http://com
*/

get_header(); ?>
			
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php the_content(); ?>


		<?php endwhile; else: ?>
	  		<p><?php _e('Sorry, this page does not exist.'); ?></p>
		<?php endif; ?>

<div class="container free-quote-cctv">
	<div class="row">
		<div class="col-md-offset-3 col-md-6">
			<div class="col-md-6">
		   		<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/image001.jpg" class="img-responsive">
		   	</div>
			
			<div class="col-md-6 text-center">
		   		<p class="lead vertical-align">Get a free quote for <br/>CCTV installation</p>
		   		<p>Peace of mind is only a few clicks away.</p>
		   		<a class="btn btn-default btn-lg">Get your free quote</a>
			</div>
		</div>
	</div>
</div>

<div class="container partnerlogos" id="partnergrid" >
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center forward our-partners-text">--- Our Partners ---</h2>
		</div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/vivotek_bw.gif" alt="" id="vivotek" class="img-responsive">
		</div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/pelco-logo-bw.gif" alt="" id="pelco" class="img-responsive">
		</div>
		<div class="col-md-3">
			<img style="z-index: -1;" src="https://www.avsecurity.com/wp-content/uploads/2014/04/Axis-Logo-200_bw.jpg" id="axis" alt="" class="img-responsive">
		</div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/Hikvision_logo-200_bw.jpg" id="hikvision" alt="" class="img-responsive">
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/geovision_bw.gif" alt="" id="geovision" class="img-responsive"></div>
		
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/exacq_bw.jpg" alt="" id="exacq" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/ganz-logo-250x150_bw.jpg" id="ganz" alt="" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/milestone-bw.jpg" alt="" id="milestone" class="img-responsive"></div>
	</div>
</div>













</div>

<?php get_footer(); ?>
    