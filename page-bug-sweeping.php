<?php get_header(); ?>
<div class="jumbotron text-center" id="electronicbugsweeping">
    <div class="container animated slideInLeft">
        <h1 class="bug-sweeping-hero text-uppercase">Bug Sweeping</h1>
        <p class="text-white">Is somebody listening in?</p>
        <a class="btn btn-default btn-lg" role="button" href="<?php
                $blog_id = get_current_blog_id();
                echo get_home_url( $blog_id, 'contact-us' ); ?>/">Get advice today</a>
    </div>
</div>
<div class="container">
<section>
<div class="row">
    <div class="col-sm-3 hidden-xs">
    <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/03/AVS-Logo.png">
    </div>
    <div class="col-sm-9">
    <h3>AV Security is Ireland’s leading counter surveillance and <acronym title="Technical Surveillance Counter-Measures">TSCM</acronym> company.</h3>
    <p class="lead">We help businesses prevent the
        loss of sensitive information from electronic surveillance and spying.</p>
    </div>
</div>
    
</section>
<section>
    <div class="row">
        <div class="col-sm-7">
            <h3>Defending against Electronic Surveillance</h3>
<p class="lead">Whether you and team are discussing potential mergers and acquisitions, company policy, marketing
        plans or new product design, you are at risk from electronic surveillance.
    </p>
    <h3>Eavesdropping Detection</h3>
                <p class="lead">
                    Electronic bugging devices are inexpensive, easy to install, physically difficult to locate and have become very much part
                    of today's commercial environment.</p>

        </div>
        <div class="col-sm-5">
            <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/05/board-room.jpg">
        </div>
    </div>
    </section>
        <h3>Counter Surveillance</h3>
    <p class="lead">We offer advanced counter surveillance bug sweeping services to all levels of industry and private homes.</p>
    <p>We work within
        offices, factories, warehousing, design centres, laboratories, trading floors and any other areas which may be the
        target of electronic surveillance or espionage. We also offer advanced bug sweeping services to individuals at their
        homes. </p>
    <p>Our professional counter surveillance service is Dublin based and is dedicated to counter surveillance sweeps and related
        risk analysis. <strong>We can attend to any need within a 24 hour period.</strong></p>
        <section class="our-tscm-surveys">
    <div class="row text-center">
        <div class="col-md-12" style="opacity:0.5;">
            <div id="our-tscm-surveys" class="page-title">
                <h2 class="tscm-surveys-header">TSCM Surveys</h2>
            </div>
        </div>
        <div class="col-md-3  col-sm-3">
            <img class="tscm-icons center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugged-business.jpg" />
            <h3 class="tscm-header">Commercial</h3>
            <ul class="list-group">
                <li class="list-group-item">Solicitors Office Inspections</li>
                <li class="list-group-item">Boardrooms EGM/AGM</li>
                <li class="list-group-item">Mergers and Take-overs</li>
                <li class="list-group-item">Conference Room Inspections</li>
                <li class="list-group-item">Board Meeting Inspections</li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-3">
            <h1><img class="tscm-icons center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugged-home1.jpg" /></h1>
            <h3 class="tscm-header">Residential</h3>
            <ul class="list-group">
                <li class="list-group-item">Homes and Offices</li>
                <li class="list-group-item">Domestic Home Inspections</li>
            </ul>
        </div>
        <div class="col-md-3 col-sm-3">
            <img class="tscm-icons center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/bugged-car.jpg" />
            <h3 class="tscm-header">Vehicles</h3>
            <ul class="list-group">
                <li class="list-group-item">GPS Tracking Device Detection</li>
                <li class="list-group-item">MD Vehicle Inpections</li>
                <li class="list-group-item">Aircraft</li>
                <li class="list-group-item">Boats</li>
            </ul>
        </div>
        <div id="cyber-and-wifi-analysis" class="col-md-3 col-sm-3">
            <img class="tscm-icons center-block" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Flaticon_326.jpg" />
            <h3 class="tscm-header">Cyber & Wifi Analysis</h3>
            <ul class="list-group">
                <li class="list-group-item">Wi-Fi Security Audits</li>
                <li class="list-group-item">Complete Building & Floor Audits</li>
                <li class="list-group-item">Compliance Surveys</li>
            </ul>
        </div>
    </div>
    </section>
    <div class="row">
        <div class="col-md-8">
            <div class="page-header">
                <h2><i class="fa fa-microphone-slash fa-lg"></i> Technical Surveillance Counter-Measures</h2>
            </div>
            <p class="lead">Our state-of-the-art technical countermeasures equipment is capable of locating and neutralizing even the most
                technically advanced electronic surveillance devices.</p>
                <p>Security sweeps are conducted outside normal working
                hours to ensure discretion and minimum workplace disruption.</p>
            <h3>How the AV Security solution works</h3>
            <ol class="lead">
                <li>Initial meeting with client.</li>
                <li>Facility inspection survey, research and reconnaissance.</li>
                <li>On client's request, a countermeasures sweep of premises will be conducted.</li>
                <li>Client is provided with Risk and Vulnerability Analysis Report.</li>
            </ol>
            <a href="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Technical-Surveillance.pdf" class="btn btn-primary btn-lg">Download our TSCM brochure</a>
            <div class="well">
                <p>If you require any further information, <a title="Contact Us" href="https://www.avsecurity.com/contact-us/">call us</a>                    and one of our technical team will be happy to answer any questions you may have. Alternatively, we can
                    arrange for one of our security assessors to visit your organisation and carry out a no obligation countermeasures
                    survey.</p>
            </div>
        </div>
        <div class="col-md-4 hidden-xs visible-lg visible-md">
            <img class="2emheadspace img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/getfile-2.jpg" alt="bug sweeper">
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <h2><i class="fa fa-lg fa-signal"></i> Wireless Network Audits</h2>
            <p class="lead">AV Security provides customers with independent, clear, wireless network audits.</p>
        </div>
        <div class="col-sm-4">
            <img id="wifi-audits-image" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/80211n_spec.jpg" alt="wifi-audits" class="2emheadspace img-responsive hidden-xs">
        </div>
        <div class="col-md-12 col-sm-8 col-xs-12">
            <p class="lead">The analysis and tests of the network are performed as follows:</p>
            <ul class="list-group">
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Identify access point hardware and configuration</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Verify encryption meets best practice</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Confirm that no unauthorized network access via wifi is possible</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Identify any "rogue" access point installations</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Exploits of known factory configuration weaknesses</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Exploits of known deficiency of identified network design</li>
                <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Run network stumblers outside of building, determine leaking coverage</li>
            </ul>
        </div>
    </div>
    <?php require_once('avs_mini_bio.php'); ?>
</div>
<?php get_footer(); ?>