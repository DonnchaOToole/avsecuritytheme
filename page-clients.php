<?php get_header(); ?>
<div class="jumbotron dark-gradient" id="clients-header">
    <div class="container animated slideInLeft">
        <h1>Our Clients</h1>
        <p></p>
        <a class="btn btn-default btn-lg" role="button" href="<?php
				$blog_id = get_current_blog_id();
				echo get_home_url( $blog_id, 'contact-us' ); ?>/">Get advice today</a>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p class="lead">Our clients include law firms, trading companies, government departments, design centres, multi-national companies,
                banks, sporting organisations and a wide section of business offices throughout Ireland.</p>
            <section>
                <h3>Counter Surveillance & Bug Sweeping</h3>
                <p>In addition to corporate support, we carry out counter surveillance and bug sweeps for individuals wishing
                    to secure their privacy and confidentiality from other interested parties.</p>
                <p>These clients include persons who may be directors or associated with companies, persons of high worth including
                    celebrities and members of parliament, those pursuing a divorce or separation, or even people who may
                    just be concerned that they may be subject to another persons spying.</p>
            </section>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="col-sm-8">
                <h2>TSCM<span class="text-muted"> Business & Corporate</span></h2>
                <p class="lead">Counter measure or counter surveillance sweeps are carried out by fully experienced professional personnel
                    with many years experience in finding and locating listening devices.</p>
                <p>We use the most up to date and advanced system equipment which is able to detect active and non active transmitters
                    in addition to the latest threat of GSM devices. Equipment used includes non linear junction detectors
                    (NLJD), sophisticated spectrum analysis, short and long duration signal analysis software, video detection
                    and specialist GSM location and tracking equipment.</p>
            </div>
            <div class="col-sm-4"><img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/business-11.jpg" alt="" class="bottompushimages img-rounded img-responsive"></div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col-sm-8">
                <h2>Counter Surveillance and Bug Sweeping<span class="text-muted"> in the Home</span></h2>
                <p class="lead">
                    Your home can often be the target of electronic surveillance, where listening bugs and video cameras can be planted with
                    surprising ease.
                </p>
                <p>
                    Many of these devices can be hidden in sockets, electronic equipment, lights and electrical wiring. They can also easily
                    be fitted to phone lines and in cavities within walls and ceilings. Depending on the level of threat,
                    we use the same counter surveillance equipment for the TSCM countermeasures sweeps in the office or business
                    workplace.
                </p>
            </div>
            <div class="col-sm-4">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/tscm-homes.jpg" alt="" class="img-responsive bottompushimages img-rounded">
            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <div class="col-sm-8">
                <h2>Counter Surveillance<span class="text-muted"> in Vehicles & Boats</span></h2>
                <p class="lead">In addition to offices, businesses and homes, it is commonplace for vehicles and yachts to be the subject
                    of surveillance and spying.</p>
                <p>Discreet TSCM personnel can visit your vehicle or yacht and carry out a counter surveillance sweep. This
                    will include a full electronic and visual inspection with special attention given to covert position trackers
                    that may be used to determine your whereabouts and journey routes.</p>
            </div>
            <div class="col-sm-4">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/dunboats.jpg" alt="" class="img-responsive bottompushimages img-rounded">
            </div>
        </div>
</div>
</section>
<?php get_footer(); ?>