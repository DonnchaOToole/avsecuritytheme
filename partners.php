<!-- PARTNERS -->
<div class="partnerlogos">
	<div class="row">
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/vivotek_bw.gif" alt="" id="vivotek" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/pelco-logo-bw.gif" alt="" id="pelco" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/Axis-Logo-200_bw.jpg" id="axis" alt="" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/Hikvision_logo-200_bw.jpg" id="hikvision" alt="" class="img-responsive"></div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/geovision_bw.gif" alt="" id="geovision" class="img-responsive"></div>
		
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/exacq_bw.jpg" alt="" id="exacq" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/ganz-logo-250x150_bw.jpg" id="ganz" alt="" class="img-responsive"></div>
		<div class="col-md-3">
			<img src="https://www.avsecurity.com/wp-content/uploads/2014/04/milestone-bw.jpg" alt="" id="milestone" class="img-responsive"></div>
	</div>
</div>