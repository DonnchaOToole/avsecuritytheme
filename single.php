<?php get_header(); ?>
<div class="container news-single-wrap 6emfootspace">
    <div class="row">
        <div class="pagecontentwrap col-sm-8">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="single-post-header-image">
                <?php 
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('full', "array('class' => 'img-responsive')");
                                } ?>
            </div>
            <div class="news-single-title page-header">
                <h1>
                    <?php the_title(); ?>
                </h1>
            </div>
            <div class="single-post-content justified">
                <?php the_content();  ?>
            </div>
        </div>
        <div id='sidebardiv' class="col-sm-4 text-uppercase">
            <?php get_sidebar(); ?>
        </div>
    </div>
    <?php endwhile; else: ?>
    <p>
        <?php _e('Sorry, this page does not exist.'); ?>
    </p>
    <?php endif; ?>
</div>

<?php get_footer(); ?>
<script>
    jQuery('.wp-post-image').addClass("img-responsive");
</script>