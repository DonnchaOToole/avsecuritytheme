		
		<h3>Recent News:</h3>
		    <ul class="list-group">
				<?php

				$args = array( 'posts_per_page' => 10, 'offset'=> 0, 'category' => 13 );

				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
					<li class="list-group-item">
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</li>
				<?php endforeach; 
				wp_reset_postdata();?>

			</ul>