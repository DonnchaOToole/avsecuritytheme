<?php get_header(); ?>
    <!--<style>
    img.home-cctv {
        max-height: 200px;
    }
    </style>-->
    <div class="jumbotron" id="homecctv">
        <div class="container animated slideInLeft">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <h1>Home CCTV</h1>
                    <!--<p>Protecting your family and your property.</p>-->
                    <button class="btn btn-default btn-lg quote-header" data-toggle="modal" data-target="#contact-modal">Get a free quote</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="page-header">
                    <h1>Home Security Cameras <small></small></h1>
                </div>
                <p class="lead">We carry out installation of security camera systems internally and externally to provide you with peace of mind on a day to day basis when you cannot be at your property, or while you and your family are asleep.</p>
                <p>We are extremely discreet and professional and can help you with planning your home security camera system. If you have any security or trust issues inside the home we can install covert cameras to reassure your concerns.</p>
                <section>
                <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/04/home-security-camera.jpg">  
                </section>
                <div class="row">
                    <div class="col-md-6">
                        <h2><i class="fa fa-lg fa-mobile"></i> Remote Viewing</h2>
                        <p>All our camera systems can be linked up to a smartphone allowing live and recorded images to be viewed remotely.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <h2><i class="fa fa-lg fa-check"></i> Easy</h2>
                        <p>Our staff are very knowledgeable and helpful and we have been providing these services to the Dublin area for over twenty years.</p>
                    </div>
                </div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-md-8">
    
<img src="https://www.avsecurity.com/wp-content/uploads/2017/04/burglar.jpg" alt="" class="burglar img-responsive">

    <section class="advantages-of-cctv">
    <h2>3 Advantages of Using CCTV System at your Home</h2>

<h3>Crime deterrent</h3>

<p>It goes without saying that having a CCTV camera installed at your Home will act as a serious deterrent to criminals and anyone carrying out illegal activities. The sight of a CCTV camera infers an air of danger and the presence of the law, deterring anyone planning to carry out a crime from doing so.</p>

<h3>Monitor activities</h3>

<p>CCTV systems are able to keep track of what is happening at the premises where they are installed. By monitoring the activity of all visitors you can have total peace of mind about exactly what is going on under your roof.</p>

<h3>Collect evidence</h3>

<p>In the unfortunate event of a crime occurring at home, having a CCTV system really does pay dividends as it provides a way of collecting evidence to help ‘suss out’ exactly what happened. Crimes can be solved far more easily with additional evidence from a CCTV camera, helping place times, locations and, most importantly, suspects.</p>
    </section>
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 2rem;">       
                        <p class="lead">Our expert team are here to provide a fast and professional response to your inquiry.</p>
                        <p>Get in touch today and we'll aim to contact you within 24 hours.</p>
                        <a href="<?php echo get_home_url( $blog_id, 'contact-us' ); ?>/" class="btn btn-primary btn-lg">Get in touch</a>
                    </div>
       
        </div>
    </div>
       
        <?php require_once('remote-viewing.php'); ?>
 <?php require_once('price-match-cta.php'); ?>
    <?php get_footer(); ?>
