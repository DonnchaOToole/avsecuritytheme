<?php get_header(); ?>
    <div class="jumbotron bg-yellow">
        <div class="container animated slideInLeft">
            <h1>Add Ons</h1>
            <button class="btn btn-default btn-lg" data-toggle="modal" data-target="#contact-modal">Get a free quote</button>
        </div>
    </div>
    <div class="container">
        <div class="row addons-blocks">
            <div class="col-sm-4">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/1.jpg" alt="add ons" />
            </div>
            <div class="col-sm-8">
                <h2>Video Analytics</h2>
                <p class="lead">Improve your security and business management.</p>
                <p>We have strong relationships with the leading CCTV software providers and can tailor a solution to your requirements.</p>
                <ul class="list-group">
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> People counting</li>
                    <li class="list-group-item"><i class="fa fa-lg fa-check"></i> Number plate recognition</li>
                </ul>
            </div>
        </div>
        <div class="row addons-blocks">
            <div class="col-sm-4">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/image001-1.jpg" alt="Remote CCTV" />
            </div>
            <div class="col-sm-8">
                <h2>Remote CCTV monitoring</h2>
                <p>AV Security Business Solutions offers remote CCTV monitoring with live audio warnings for all types of commercial business.
                </p>
            </div>
        </div>
        <div class="row addons-blocks">
            <div class="col-sm-4">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Accessc.jpg" alt="Controlled Entry" />
            </div>
            <div class="col-sm-8">
                <h2>Access control</h2>
                <p>AV Security can install access control systems, from keypads and magnetic swipes to biometric fingerprint readers.</p>
            </div>
        </div>
        <div class="row addons-blocks">
            <div class="col-sm-4">
                <div class="vid">
                    <iframe width="480" height="360" src="//www.youtube.com/embed/hjgC7pBOqpw" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-sm-8">
                <h2>Till Scan & Point Of Sale CCTV</h2>
                <p class="lead">Till scan is a fraud detection system linking the cash register to the CCTV system at the premises.</p>
                <p>All cash register transactions are digitally recorded in detail, including the item description and price. The cash register is fitted with an electronic interface (where compatible) to relay the detailed information of each transaction to the CCTV system. An overhead covert camera records the actions of the staff member during the transaction.</p>
            </div>
        </div>
        <div class="row addons-blocks">
            <div class="col-sm-4">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/Online-Cloud-Storage.jpg" alt="add ons" />
            </div>
            <div class="col-sm-8">
                <h2>Cloud Storage</h2>
                <p>CCTV recordings are automatically uploaded to dedicated off-site video cloud servers.
                </p>
            </div>
        </div>
    </div>
    <?php get_footer() ?>;
