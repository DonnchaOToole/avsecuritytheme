<?php get_header(); ?>
    <div id="areasatrisk" class="jumbotron">
        <div class="container animated slideInLeft">
            <h1>Bugging Threats</h1>
            <p>Think there might be someone listening in?</p>
            <a class="btn btn-default btn-lg" href="<?php bloginfo('url');?>/contact-us/">Get in Touch »</a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <img id="mobilephonehackingimg" class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/iOS-7-Apple-page-578-80.jpg" alt="" />
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="page-header">
                    <h2>Voice Mail Hacking</h2>
                </div>
                <p class="lead">Mobile phone voice mail hacking occurs when a hacker dials into retrieve voice mail messages from phones which still have <strong>factory set</strong> PIN numbers. </p>
                <p>A survey done recently showed that less than half of all customers in the survey used keypad locks or passwords to secure their mobile phones. Have you changed yours?</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <div class="page-header">
                    <h2>Mobile Phones as Bugs</h2>
                </div>
                <p class="lead">Mobile phones themselves could be used as bugging devices. They can be programmed not to vibrate, ring or show any outward signs that they are being called. They then auto-answer and the caller can listen in to conversation within the room.</p>
                <p>This type of attack can either be done on a target phone which the buggist would then leave somewhere in the vicinity of the conversation they are targeting. Or it could be done illicitly by sending someone an email or text with an attachment which, once opened, would download the necessary software onto the target phone. The phone can then be controlled by the hacker who can then remotely switch it on and be listened to from any other mobile phone anywhere in world. </p>
                <p>In addition, a report by Computerworld claims that some new smartphone apps are using your phones microphone and camera to gather creepy data about you! A new class of app has emerged that uses the microphone built into your phone as a covert listening device. The apps try to alleviate privacy concerns by saying they only record sound patterns, not actual sounds or conversations. But in the end, the technology is there, and it’s being used to some extent.</p>
                <p>As a precaution you should take care when downloading apps as you could easily download malware and only download from your service providers website. An be cautious of using Wi-Fi connections. Unsecured networks could mean that someone else using that network could see what you’re doing on your phone. </p>
            </div>
            <div class="col-md-4 col-sm-4 hidden-xs">
                <img id="mobile-phone-as-bugs-image" class="img-responsive img-rounded" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/HTCONESilver_Left_BIG.jpg" alt="" />
            </div>
        </div>
        <section>
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img class="img-responsive" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/camera-icon.jpg" alt="mobile phone bugs">
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="page-header">
                    <h2>Mobile Phone Cameras</h2>
                </div>
                <p class="lead">Even when a GSM function is disabled on a smartphone, the in-built camera can still be used as an eavesdropping device.  </p>
                <p>Smartphones can be used to record videos/photos, stealing product information prototypes etc. As it is impossible buy a mobile phone without a camera, this is a concern which should not be ignored.</p>
            </div>
        </div>
        </section>
        <div class="row">
            <div class="col-sm-8">
                <div class="page-header">
                    <h2>GSM Listening Devices</h2>
                </div>
                <p>GSM bugs use mobile phone networks to enable the user to dial in to the bug from anywhere in the world in order to listen in to what is going on around the unit. Dial in to the bug from anywhere in the world and listen in to what is going on around it. The microphone is very powerful and will pick up conversations and background noises.</p>
                <p>A GSM bug can be a small black box that can be discreetly hidden in a room or it can be a purpose built device such as a mains adaptor, a power strip adaptor, a PC mouse or a phone charger for example. These are everyday objects that one would expect to find in an office.</p>
            </div>
            <div class="col-sm-4">
                <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/E00961.jpg" class="img-responsive" alt="GSM listening device">
            </div>
        </div>
        <!-- ALL ABOUT PHONE TAPS COLLAPSE AREA -->
        <div class="page-header">
            <h2>More information:</h2>
        </div>
        <div id="accordion" class="panel-group">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						<i class="margin-right-8 fa fa-lg fa-info"></i> Phone Taps and Phone Bugs
					</a>
				</h3>
                </div>
                <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body">
                        <p class="lead">Two terms which are often interchanged but in fact refer to two separate types of attack are Bug and Tap. The differences are:</p>
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/areas-at-risk-old-style-phone-avsecurity-300x256.jpg" class='img-responsive img-circle'>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <h4>Tap or Wiretap</h4>
                            <p>This is a device placed on the telephone system and is designed to intercept telephone conversations. I.e. if no call is in place then the tap is inactive.</p>
                            <h4>Bug</h4> A bug is used to listen to room conversation but may use the telephone/line as a facilitator of this. This type of attack means that all conversation in the area is susceptible to being overheard by the attacker and not just telephone calls. Other forms of telephone (landline) attack are:
                            <h4>Spare Pair</h4> There can be several unused wires in the telephone cable, (e.g. a typical analogue telephone will use two wires (one pair) and any other conductors in the cable will be unused or spare). These unused wires could be attacked and used for eavesdropping purposes. The simplest form of attack using a spare pair would be to connect a microphone to an unused pair; the wire would then be traced to a convenient listening point where, by the connection of a suitable amplifier, the audio could be picked up. Alternatively a recorder could be connected in place of the amplifier and the recordings could be listened to after the event. Another option would be to use a micro phonic device (microphone or loudspeaker) within the telephone itself and to route this signal via an unused pair in a similar way.
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/areas-at-risk-split-resplit-avsecurity-302x200.jpg" class='img-responsive img-circle'>
                        </div>
                        <div class="col-md-8 col-xs-8">
                            <h4>Split/Re-split</h4> A rather complicated attack, the split/re-split. This exploits the fact that un-screened cables can radiate signals which in turn can be coupled to a second wire. A signal being transmitted along the grey wire will be fed along the first red link (this is the initial split) and end up on the green wire of the spare pair of wires. This will in turn be inductively coupled onto the yellow wire. At a point further down the cable the green wire is re-split back to the original grey conductor. The result is that at the far end of the blue/grey pair the original signal is present as intended but at the far end of the green/yellow pair, the same signal is also present. The split/re-split distance should be at least 15-20 metres apart in order for sufficient signal to be induced from the original conductor to the spare pair.
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					<i class="fa fa-lg margin-right-8 fa-info"></i> Listening Devices
				</a>
			</h3>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img id="hookswitch-image" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/hook_fig1.gif" alt="placeholder+image" />
                            </div>
                            <div class="col-md-9">
                                <h3>Hookswitch Bypass Listening Devices</h3>
                                <p>This form of attack is a modification of the analogue telephone unit itself. As previously described, when the telephone line is active, the DC voltage on the line drops and audio is passed along the line. When the receiver is replaced, the hookswitch is opened and the voltage goes high and no audio will pass down the line. In a hookswitch bypass attack, the telephone hookswitch is modified so it appears to be in an off hook (line active) condition, even with the receiver (handset) still on hook. The disadvantage with this is that the telephone, when dialled in to, would appear to be in use. A more advanced attack would be to modify the hookswitch circuitry such that the voltage was dropped sufficiently for the equipment to make the microphone and line active and therefore pass audio. But, at the same time not dropping the voltage low enough for the phone to appear engaged (busy) if anyone were to dial into it.</p>
                            </div>
                        </div>
                        <!-- END ROW -->
                        <h3>Passive Audio Listening Devices</h3>
                        <p>This is a simple attack whereby a microphone (either the telephone mic or a secondary microphone installed by the attacker) is connected to a pair of wires and the unit thereby used to monitor room audio and pass it to a distant listening post situated somewhere on the line.</p>
                        <h3>Digital Audio Listening Devices</h3>
                        <p>Similar to the Passive attack described above but used in a digital telephone. This would require the appropriate audio decoder to be used dependant on the make/model of the telephone being attacked.</p>
                        <h3>Free Space RF</h3>
                        <p>A free space transmitter is very similar to that used as a room bug but is concealed within the telephone equipment itself. This type of attack could use its own microphone or it may utilise telephone’s microphone.</p>
                        <h3>Carrier Current Device</h3>
                        <p>Carrier current is a method of low power AM radio transmission that uses the AC electrical system of a building to propagate a medium frequency AM signal to a relatively small area, such as a building or a group of buildings. In the some countries, carrier current stations do not require a broadcasting license.</p>
                        <h3>Infinity Transmitter Listening Device</h3>
                        <p>An infinity transmitter (also known as a harmonica bug) is a surveillance device used to covertly monitor conversation in a room through a telephone line. Its name derives from the fact that, by using a telephone line as a transmitter, it can work at an infinite distance, unlike other bugging devices that have only a finite signal range. The alternative name 'harmonica bug' refers to the fact that such devices were originally activated using the tone produced by a harmonica. Not to be confused with mobile telephone type of bugs e.g. GSM bugs.</p>
                        <h3>Radio (RF) Bugging Device</h3>
                        <p>A RF(Radio Frequency) bug involves the placing of a radio transmitter in a room. One of the most infamous examples of the use of a RF bug is the “Great Seal Bug” story, when in 1952 a RF listening device was found in a carved wooden seal that had been presented to the US Embassy in Moscow and had hung in the Embassy since 1946. RF bugs can be incredibly small and can be concealed in just about anything including skirting boards, picture frames, plugs etc. Radio frequencies are given off by nearly all spying devices, and these radio frequencies can be detected with the proper equipment. Different types of bugs give off a large range of frequencies and specialist equipment is required to check the entire RF spectrum. Basic RF detectors, that can be purchased relatively cheaply, will only be able to detect limited frequencies and will give you a false sense of security.</p>
                    </div>
                    <!-- END PANEL BODY -->
                </div>
            </div>
            <!-- END PANEL -->
            <div class="panel panel-primary">
                <!-- START PANEL -->
                <div class="panel-heading">
                    <h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					<i class="fa fa-lg fa-info margin-right-8"></i> Other security risks</h4>
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-md-3 col-xs-3">
                            <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/areas-at-risk-lady-usb-flash-drive-into-laptop-avsecurity-300x199-1.jpg" alt="img-responsive">
                        </div>
                        <div class="col-md-9 col-xs-9">
                            <h3>In Built Vulnerabilities</h3> Many modern telephones, particularly digital and VoIP are controlled by software applications and have any features over and above the basic operational ones. These features may be hands free operation, auto answer, call alert etc. Whilst these are very handy features, anyone with knowledge of the individual software applications and time in the building can use these features to listen in to calls without the user’s knowledge. Access to the building may not be required if the telephone system is on a server with internet connection and remote dial in is active. It is vitally important that the telephone administrators can be trusted as they are the people that understand the system better than most and could be coerced into mounting an attack for a third party. With their extensive knowledge, staff such as these system administrators could also overwrite the system logs in order to prevent their activities being discovered.
                            <h3>Keystroke Logging</h3> Keystroke logging, often referred to as keylogging, is the action of recording (or logging) the keys struck on a keyboard, typically in a covert manner so that the person using the keyboard is unaware that their actions are being monitored. There are numerous keylogging methods, ranging from hardware and software-based approaches to acoustic analysis. USB flash drives can be used to capture potentially sensitive material like bank or credit card details.
                            <h3>Tracking devices (GPS)</h3> GPS or Global Positioning System, is a satellite navigation system which can pin-point where a GPS receiver is by ascertaining its latitude and longitude. Satellites around the earth transmit radio signals which can help identify the precise locations of a vehicle equipped with a GPS receiver. Data pushers are types of GPS systems in which the receiving unit sends data from the device to a central database at regular intervals, updating information on location, direction, speed and distance. 
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-xs-3">
                                <img class="wp-image-1548 alignright" src="<?php bloginfo('url');?>/wp-content/uploads/2013/12/pir.jpg" alt="pir" width="120" height="120" />
                            </div>
                            <div class="col-md-9 col-xs-9">
                                <h3>Covert Cameras</h3>
                                <p>A hidden camera is a still video camera used to film people without their knowledge. The camera is "hidden" because it is either not visible to the subject being filmed, or is disguised as another object. Hidden cameras have become popular for Business surveillance, and can be built into common household objects such as smoke detectors, clock radio, motion detectors and mobile phones. Hidden cameras may also be used commercially or industrially as security cameras.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PANEL		 -->
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
