<?php get_header(); ?>

<div class="container product-single-wrap">
	<div class="row">
		<div class="pagecontentwrap col-md-12">
				
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				
				<div class="page-header">
					<h1 id="product-single-title"><?php the_title(); ?></h1> 
				</div>
				<div class="row">
					<div class="offset-md-4 col-md-4">
						<div class="img-responsive panel panel-default img-rounded single-post-header-image">	
						<?php
						if ( has_post_thumbnail() ) {
						the_post_thumbnail('full');
						} ?>
						</div>
					</div>
					<div class="col-md-8">
						<div class="lead">
							<?php the_excerpt(); ?>
						</div>
						<div class="product-content single-post-content col-md-12">				
								<?php the_content(); ?>
						</div>
					</div>
				</div>
			
			<a class="btn btn-primary btn-lg" role="button" href="
			<?php
			$blog_id = get_current_blog_id();
			$path = "product";
			$products_link = get_home_url( $blog_id, $path );
			echo $products_link;
			?>
			">
			<i class="fa fa-lg fa-arrow-left" style="color:white;"></i> Back to products
			</a>
			<a style="margin-left=1em;" class="pull-right btn btn-success btn-lg" role="button" href="
			<?php
			$blog_id = get_current_blog_id();
			$path = "contact-us";
			$products_link = get_home_url( $blog_id, $path );
			echo $products_link;
			?>
			">
			Get in touch <i class="fa fa-lg fa-arrow-right" style="color:white;"></i> 
			</a>
				

			<?php endwhile; else: ?>
		  		<p><?php _e('Sorry, this product does not exist.'); ?></p>
			<?php endif; ?>
				
		</div>
	</div>
</div>


<?php get_footer(); ?>
<script>
jQuery('.wp-post-image').addClass("img-responsive");
</script>    
