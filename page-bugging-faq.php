<?php get_header(); ?>
<div class="jumbotron hidden-xs" id="bugging-faq">
    <div class="container animated slideInLeft">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 text-center">
                <h1 style="color:white;">Bugging FAQ</h1>
                <p class="lead text-white">Frequently Asked Questions</p>
                </p><a class="btn btn-default btn-lg" href="<?php
                $blog_id = get_current_blog_id();
                echo get_home_url( $blog_id, 'contact-us' ); ?>/" role="button">Get in Touch »</a>
            </div>
        </div>
    </div>
</div>
<div class="jumbotron visible-xs" id="bugging-faq-xs">
    <div class="container animated slideInLeft">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 text-center">
                <h1 style="color:white;">Bugging FAQ</h1>
                </p><a class="btn btn-default btn-lg" href="<?php
                $blog_id = get_current_blog_id();
                echo get_home_url( $blog_id, 'contact-us' ); ?>" role="button">Get in Touch »</a>
            </div>
        </div>
    </div>
</div>
<div id="bugging-faq-wrap" class="container">
    <div class="row questions">
    <div class="col-sm-5"><img src="https://www.avsecurity.com/wp-content/uploads/2017/05/who.jpg" alt="" class="img-responsive"></div>
        <div class="col-sm-7">
            <h3>Who engages in bugging?</h3>
            <p>Its difficult to quantify the precise level of threat, however information gleaned from various sweep teams indicates
                <strong>around 45% of bugging incidents are internal.</strong></p>
            <p>Directors bugging other directors, bosses bugging employees, spy cameras, phone taps and listening devices etc.</p>

            <p>Definitions are tricky, if an employer states in a contract of employment that telephones may be recorded, is
                that telephone tapping? Probably not.</p>
        </div>
    </div>
    <div class="row questions">
    <div class="col-sm-5"><img src="https://www.avsecurity.com/wp-content/uploads/2017/05/board.jpg" alt="" class="img-responsive"></div>
        <div class="col-sm-7">
            <div class="page-title">
                <h3>When are companies most at risk?</h3>
            </div>
            <p>
                When business expansion or rationalisation plans are being discussed. New products or marketing initiatives are being developed.
                Acquisition or mergers are being planned, or perhaps work is in a sensitive related industry. Of course,
                this also applies to legal and financial advisors party to the same information, if senior executives or
                customers are subject to media attention.
            </p>
        </div>
    </div>
        <div class="row questions">
        <div class="col-sm-5"><img src="https://www.avsecurity.com/wp-content/uploads/2017/05/signals-detection.jpg" alt="" class="img-responsive"></div>
            <div class="col-sm-7">
            
                    <h3>Why is electronic eavesdropping detection a standard practice?</h3>
                    <p class="lead">There are several reasons:</p>
                            <ul>
                    <li>Protection of profits.</li>
                    <li>Protection of strategies.</li>
                    <li>Protection of intellectual assets.</li>
                    <li>Protection of personal privacy and safety.</li>
                    <li>A fiduciary responsibility to stockholders.</li>
                    <li>Prevention is far more cost effective than sustaining losses.</li>
                    <li>Espionage is a grow questionsth industry.</li>
                </ul>
            </div>
        </div>
    <div class="row questions">
    <!--<div class="col-sm-5"><img src="https://placehold.it/500x500" alt="" class="img-responsive"></div>-->
            <div class="col-sm-7">
                <div class="page-title">
                    <h3>Why have a TSCM inspection?</h3>
                </div>
                <ul>
                    <li>Inspections solve current problems.</li>
                    <li>Inspections limit future windows-of-vulnerability.</li>
                    <li>Inspections help satisfy on-going legal requirements for due diligence.</li>
                    <li>Inspections help establish the legal eligibility requirement for business secret status in court.</li>
                    <li>Inspections help protect individual privacy and personal safety.</li>
                </ul>
            </div>
    </div>
        <div class="row questions">
        <!--<div class="col-sm-5"><img src="https://placehold.it/500x500" alt="" class="img-responsive"></div>-->
            <div class="col-sm-7">
                <h3>How often are inspections conducted?</h3>
                <p>The generally accepted business practice is quarterly, although some of our clients have requirements for
                    monthly inspections.
                </p>
            </div>
        </div>
<?php require_once('avs_mini_bio.php');?>
</div>
<?php get_footer(); ?>