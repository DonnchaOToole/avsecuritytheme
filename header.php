<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Preventing fraud and breaches of security for Irish businesses and homes for over 20 years.">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
    <meta name="google-site-verification" content="-hViKaBT66f10yNBjUXPXYZO6bp0J2UJg2Dkt3EWeSA" />
<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
<link rel="manifest" href="/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">    

 <!--<meta http-equiv="refresh" content="10">-->


    <title>
        <?php wp_title('|',1,'right'); ?>
    </title>
    <script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-41266755-1', 'auto');
    ga('send', 'pageview');
    </script>
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
    <?php wp_enqueue_script("jquery"); ?>
        <?php wp_head(); ?>
</head>
<style>
a.btn#navbar-call-button-sm:hover {
    color: white;
}

i.fa-phone:hover {
    color: white;
}
</style>

<body>
    <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
    <nav class="navbar navbar-inverse navbar-fixed-top avsnav navbar-avs" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" id="navbartoggleswitch" class="navbar-toggle" data-toggle="collapse" data-target="#avs-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand hidden-sm hidden-md" href="<?php echo home_url(); ?>">
                    <img target="_blank" id="navbarbrandlogo" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/av-security-logo-small-e1398951521107.png" alt="avsecuritylogo">
                    <!-- <?php bloginfo('name'); ?> -->
                </a>
                <a class="navbar-brand visible-md visible-sm" href="<?php echo home_url(); ?>">
                    <img target="_blank" id="navbarbrandlogo-sm" src="<?php bloginfo('url');?>/wp-content/uploads/2014/05/av-security-logo-small-e1398951521107.png" alt="avsecuritylogo">
                </a>
                 <a class="btn btn-default visible-xs visible-sm" style="float:right;" id="navbar-call-button" href="tel:0035312870055"><i id="navbar-call-button-phone" class="fa fa-lg fa-phone"></i></a> 
            </div>
            <!-- call buttons -->
            <ul class="hidden-xs hidden-sm hidden-md nav navbar-nav navbar-right">
                <li id="navbarcontact" style="margin-top: 5px;">
                    <h3><a class="btn call-button btn-default btn-lg" href="tel:0035312870055"><i id="phone-beside-number" class="fa fa-lg fa-phone"></i>+353 1 287 0055</a></h3></li>
            </ul>
            <ul class="visible-md hidden-sm nav navbar-nav navbar-right">
                <li>
                    <a class="btn btn-default" id="navbar-call-button-sm" href="tel:0035312870055"><i class="fa fa-lg fa-phone"></i></a>
                </li>
            </ul>
            <?php
                wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'primary',
                    'depth'             => 2,
                    'container'         => 'div',
                    'container_class'   => 'collapse navbar-collapse',
                    'container_id'      => 'avs-nav',
                    'menu_class'        => 'nav navbar-nav',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </div>
    </nav>
    <?php $blog_id = get_current_blog_id(); ?>
