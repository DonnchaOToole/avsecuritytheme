<?php get_header(); ?>
    <div class="bluegradient jumbotron">
        <div class="page-header">
            <h1>Get a quote</h1>
        </div>
    </div>
    <div class="container" id="contact-us-form">
        [cscf-contact-form]
        <hr/>
    </div>
    <?php get_footer(); ?>
