<?php get_header(); ?>
<div id="securitycamerasystems" class="jumbotron dark-gradient">
    <div class="container animated slideInDown">
        <h1>Security Camera Systems</h1>
        <a href="<?php
				$blog_id = get_current_blog_id();
				echo get_home_url( $blog_id, 'contact-us' ); ?>" class="btn btn-default btn-lg">Get in touch</a>
    </div>
</div>
<div class="container marketing">
    <div class="row">
        <div class="col-md-3"><img class="size-full wp-image-1565" src="<?php bloginfo('url');?>/wp-content/uploads/2013/12/hik-camera.jpg" alt="hik camera"
            /></div>
        <div class="col-md-9">
            <p class="lead">Security cameras are changing rapidly – new technology is making systems easier to operate, integrate and use.
                We can help you by installing the most up to date equipment with the latest technology. We can also upgrade
                your existing security camera system from analogue to a digital IP camera systems.</p>
        </div>
    </div>
</div>
<div class="container">
<h2>Available CCTV Systems</h2>
    <div class="row">
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Analogue Security Camera System</h3>
                </div>
                <div class="panel-body">Analogue CCTV is still popular for budget installations.</div>
            </div>
        </div>
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">HD CCTV Camera System</h3>
                </div>
                <div class="panel-body">High-Definition with superior image quality and high pixel density.</div>
            </div>
        </div>
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">IP CCTV Camera</h3>
                </div>
                <div class="panel-body">Digital CCTV is high-tech, scalable and the live feed can be viewed on your smartphone.</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Hidden Covert Cameras</h3>
                </div>
                <div class="panel-body">Till transactions are recorded with relevant CCTV footage for prevention of till fraud.</div>
            </div>
        </div>
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Automatic Numberplate Recognition Systems</h3>
                </div>
                <div class="panel-body">AKA ANPR, allows you to monitor and identify vehicles.</div>
            </div>
        </div>
        <div class="cameragrid col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Remote Access Security Camera Systems</h3>
                </div>
                <div class="panel-body">Allows you to view a live feed on your smartphone, tablet or pc. It's as easy as launching the app.</div>
            </div>
        </div>
    </div>
   </div>
        <?php require_once('remote-viewing.php'); ?>

<?php get_footer(); ?>
