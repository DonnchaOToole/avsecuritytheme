<?php get_header(); ?>
<div id="commercial-cctv-header" class="jumbotron text-white">
    <div class="container animated slideInLeft">
        <div class="row">
            <div class="col-md-12 col-sm-12 page-hero">
                <h1>Commercial CCTV</h1>
                <p class="lead text-white">Who is keeping an eye on your business?</p>
                <button class="btn btn-default btn-lg" data-toggle="modal" data-target="#contact-modal">Get a free quote</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <section class="cctv-installation">
        <div class="row">
            <div id="commercial-cctv-intro" class="col-sm-8">
                <h2>CCTV Installation</h2>
                <p class="lead">For expert CCTV installations in the Dublin area, look no further than AV Security. We pride ourselves on
                    offering a first-class installation service, with the latest state of the art equipment available to
                    suit the requirements of your business.</p>
                <p>With over 21 years’ experience in the CCTV industry, our certified team of installation engineers and technicians
                    are fully qualified to ensure a high standard of workmanship is achieved on every project, and are always
                    on hand to provide any advice you may require as to the best system for your needs.</p>
                <p>We offer a wide range of CCTV systems, from digital video recording to remote monitoring.</p>
            </div>
            <!--<div class="col-sm-4">
                <img style="max-height: 320px;" class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/04/hikvision.jpg" alt="">

            </div>-->
        </div>
    </section>
</div>
<section class="deterrance">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2 class="text-white">Effective Deterrance</h2>
                <p class="lead text-white">
                    AV Security CCTV systems provide an excellent deterrent against theft and vandalism.</p>
                <p class="text-white">Our CCTV systems can be used for recording the activity occurring both inside and outside your premises.
                </p>
            </div>
            <div class="col-sm-3 hidden-xs">
                <img src="https://www.avsecurity.com/wp-content/uploads/2017/03/cctv-in-operation.jpg" class="img-responsive">
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <h2>CCTV control equipment</h2>
            <p class="lead">AV Security provides an extensive range of commercial CCTV control equipment tailored to the individual needs
                of your business.</p>
            <p>From traditional analogue CCTV systems to the very latest digital IP CCTV surveillance equipment, we offer the
                perfect closed circuit television security system to meet your business security needs. </p>
        </div>
        <div class="col-md-5">
            <img class="img-responsive" src="https://www.avsecurity.com/wp-content/uploads/2017/03/cctv-control-room-e1490622854923.jpg" />
        </div>
    </div>
    <section class="cctv-range">
        <div class="row">
            <div class="col-sm-7">
                <h2>A wide range of CCTV cameras</h2>
                <p class="lead">Whether your business requires internal or external CCTV, AV Security has a camera designed to perfectly
                    integrate with your system.</p>
                <p class="lead">A professionally installed camera system can reduce shoplifting by up to <span class="bold">90%.</span></p>
            </div>
            <div class="col-sm-5">
                <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-dome.jpg" class="img-responsive"></div>
                <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/05/ptz-thermal.jpg" class="img-responsive"></div>
                <div class="col-xs-4"><img src="https://www.avsecurity.com/wp-content/uploads/2017/03/4k-bullet.jpg" class="img-responsive"></div>
            </div>
        </div>
    </section>
    <section class="low-light">
        <div class="row">
            <div class="col-sm-6">
                <h3>Low light cameras</h3>
                <p class='lead'>Industry leading low-light specifications.</p>
                <p>Including large 2 MP progressive scan CMOS image sensors, full HD 1080p video at up to 60 fps, triple video
                    streams, 3D DNR and 120dB WDR. The result is crystal-clear color images down to as low as 0.001 Lux and
                    B/W to 0.0001 Lux for sharp color images in conditions that would defeat conventional low-light models.
                    The eight-strong DarkFighter range now consists of two box cameras, two bullet cameras, two outdoor dome
                    cameras and a pair of 23X network PTZ dome cameras, five of which come with vandal-proof housings.
                </p>
            </div>
            <div class="col-sm-6">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/7iF20BS8LP4"></iframe>
                </div>
            </div>
    </section>

    <section class="low-light">
        <div class="row">
            <div class="col-sm-6">
                <h3>High contrast light cameras</h3>
                <p>Hikvision’s LightFighter camera line offers the flexibility needed to overcome even the highest contrast
                    environments. Engineered with industry-leading 140dB WDR technology, LightFighter cameras see through
                    strong lights to produce crystal-clear images with true color reproduction.</p>
            </div>
            <div class="col-sm-6">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Nzn8VlRNdFU"></iframe>
                </div>
            </div>
    </section>

    <section class="low-light">
        <div class="row">
            <div class="col-sm-6">
                <h3>Business Intelligence</h3>
                <p>Tailored for the retail industry, Hikvision has designed a central management system that brings together
                    video and POS systems. The remarkable system employs video analytics such as face recognition, people
                    counting and heat maps to monitor customer flow, preferences and employee performance. Generate a host
                    of graphic reports to capture valuable business intelligence and improve management efficiency.</p>
            </div>
            <div class="col-sm-6">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/2l6vn_ULWi0"></iframe>

                </div>
            </div>
    </section>

    <section class="low-light">
        <div class="row">
            <div class="col-sm-6">
                <h3>Smart Shop Solution</h3>
                <p>Hikvision Smart shop solution is a comprehensive IP system dedicated to security and productivity. It protects
                    staff’s security and assets, and prevents internal theft. This system also features productivity evaluation,
                    merchandising optimization, and multi-spot management. This wonderful solution is designed for independent
                    shops, department stores, and chain stores.</p>
            </div>
            <div class="col-sm-6">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/BOsLRaRbKFI"></iframe>

                </div>
            </div>
        </div>
    </section>
    </div>
    <!--<div class="container">
        <section>
            <div class="row">
                <div class="col-md-12 text-center security-systems">
                    <h2>Security Systems</h2>
                </div>
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Analogue Security Camera Systems</h3>
                        </div>
                        <div class="panel-body">
                            Analogue CCTV is still popular for budget installations.
                        </div>
                    </div>
                </div>
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">HD CCTV Camera Systems</h3>
                        </div>
                        <div class="panel-body">
                            High-Definition with superior image quality and high pixel density.
                        </div>
                    </div>
                </div>
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">IP CCTV Cameras</h3>
                        </div>
                        <div class="panel-body">
                            Digital CCTV is high-tech, scalable and the live feed can be viewed on your smartphone.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Hidden Covert Cameras</h3>
                        </div>
                        <div class="panel-body">
                            Till transactions are recorded with relevant CCTV footage for prevention of till fraud.
                        </div>
                    </div>
                </div>
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Automatic Numberplate Recognition Systems</h3>
                        </div>
                        <div class="panel-body">
                            Also known as ANPR, these systems allow you to monitor and identify vehicles.
                        </div>
                    </div>
                </div>
                <div class="cameragrid col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Remote Access Security Camera Systems</h3>
                        </div>
                        <div class="panel-body">
                            Allows you to view a live feed on your smartphone, tablet or pc. It's as easy as launching the app.
                        </div>
                    </div>
                </div>
            </div>
    </div>

    </section>-->


    <section class="repairs">
        <div class="container">
            <div class="text-center">
                <h2>Repairs &amp; Maintenance</h2>
                <p class="lead">Any repairs and maintenance can be handled by our technicians.</p>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <ul class="list-group">
                                <li class="list-group-item"><i class="fa fa-check"></i> Free security audits and advice.</li>
                                <li class="list-group-item"><i class="fa fa-check"></i> Single & multiple camera systems</li>
                                <li class="list-group-item"><i class="fa fa-check"></i> Networked & standalone systems</li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul class="list-group">
                                <li class="list-group-item"><i class="fa fa-check"></i> Remote off-site monitoring</li>
                                <li class="list-group-item"><i class="fa fa-check"></i> Data protection compliant</li>
                                <li class="list-group-item"><i class="fa fa-check"></i> System maintenance and repairs</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container">

        <section class="pay-monthly">
            <div class="text-center">
                <h2>No Upfront Costs</h2>
                <p class="lead">Pay monthly</p>
                <a href="<?php echo get_home_url();?>/contact/" class="btn btn-lg btn-primary">Get in touch</a>
            </div>
        </section>


    </div>
    <div class="container">
        <div class="partnerlogos" id="partnergrid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title">
                        <h2 class="text-center forward our-partners-text">Our Partners</h2>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/vivotek_bw.gif" alt="" id="vivotek" class="img-responsive partner-logo">
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/pelco-logo-bw.gif" alt="" id="pelco" class="img-responsive partner-logo">
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img style="z-index: -1;" src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Axis-Logo-200_bw.jpg" id="axis" alt=""
                        class="img-responsive partner-logo">
                </div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/Hikvision_logo-200_bw.jpg" id="hikvision" alt="" class="img-responsive partner-logo">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/geovision_bw.gif" alt="" id="geovision" class="img-responsive partner-logo"></div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/exacq_bw.jpg" alt="" id="exacq" class="img-responsive partner-logo"></div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/ganz-logo-250x150_bw.jpg" id="ganz" alt="" class="img-responsive partner-logo"></div>
                <div class="col-sm-3 col-xs-6">
                    <img src="<?php bloginfo('url');?>/wp-content/uploads/2014/04/milestone-bw.jpg" alt="" id="milestone" class="img-responsive partner-logo"></div>
            </div>
        </div>
        <!--<div class="col-md-12 text-center">
            <div class="" id="financingavailable">
                <a class="btn btn-lg btn-primary" style="margin-top:3em; margin-bottom:3em;" href="
                        <?php
                        $blog_id = get_current_blog_id();
                        echo get_home_url( $blog_id, 'contact-us' ); ?>" role="button
                        "><i class="fa fa-lg fa-euro" style="color:white;"></i> Financing available</a>
            </div>
        </div>-->
        <section class="did-you-know">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="text-muted text-center uppercase page-header">Did you know?</h3>
                        </div>
                        <div class="col-md-4">
                            <p class="lead">Almost 80% of theft is attributable to customers or staff, with customers accounting for the
                                largest amount.
                            </p>
                        </div>
                        <div class="col-md-4">
                            <p class="lead">A properly installed and managed security camera system can dramatically reduce crime levels.</p>

                        </div>
                        <div class="col-md-4">
                            <p class="lead">The cost of CCTV equipment has fallen steadily over recently years and performance has improved
                                significantly.
                            </p>
                        </div>
                    </div>
                </div>
        </section>

        </div>
        <?php require_once('remote-viewing.php'); ?>
        <?php require_once('price-match-cta.php'); ?>
        <?php get_footer(); ?>